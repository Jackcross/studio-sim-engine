#include "EngineManage.h"


void EngineManage::LoadContent()
{
	ScreenObjects = new ObjectsOnScreen();
	//tileMap = new TileMap(52, 29, 0.59);
	tileMap = new TileMap(52, 29, 0.40);
	menu = new Menus(ScreenObjects, tileMap);
	menu->SetEevnt(event);
	menu->ResizeToWindow(window);
}
void EngineManage::Update(double deltaTime)
{

	sf::Vector2i NewMousePosition = sf::Mouse::getPosition(*window);
	sf::Vector2f worldPos = window->mapPixelToCoords(NewMousePosition);//get world position of mouse
	menu->update(worldPos, deltaTime);

	//system("cls");
	//std::cout << worldPos.x << "  " << worldPos.y;

	if (menu->GetEngineState() == PLAY)
	{
		if (ScreenObjects->GetAllEntities().size() > 0)
		{
			if (newGame == nullptr)
			{
				newGame = new GameMain(ScreenObjects->GetAllEntities(), tileMap);
			}
			sf::Clock clock; // starts the clock

			newGame->update(deltaTime, worldPos);

		}
	}
	else
	{
		ScreenObjects->Update(worldPos, event, deltaTime);
	}

	if (menu->GetEngineState() == STOP)
	{
		if (newGame != nullptr)
		{
			newGame = nullptr;
			delete newGame;
		}
	}

}

void EngineManage::Draw()
{
	window->clear(sf::Color::Black);
	tileMap->DrawTileMap(window, event);

	if (menu->GetEngineState() == PLAY && menu->GetEngineState() != STOP)
	{
		if (ScreenObjects->GetAllEntities().size() > 0)
		{
			if (newGame != nullptr)
			{
				newGame->Draw(window);
			}
		}
	}
	else
	{
		ScreenObjects->Draw(window);
	}
	menu->Draw(window);

	window->display();
}



EngineManage::EngineManage(int argc, char* argv[])
{
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);


	window = new sf::RenderWindow(sf::VideoMode(width, height), "Studio Engine");

	float deltaTime = 0.0f;
	sf::Clock clock;
	event = new sf::Event();
	LoadContent();

		while (window->isOpen())
		{
			deltaTime = clock.restart().asSeconds();
			event;
			while (window->pollEvent(*event))
			{
				if (event->type == sf::Event::Resized)
				{
					sf::FloatRect visibleArea(0, 0, event->size.width, event->size.height);
					window->setView(sf::View(visibleArea));
					if (menu)
					{
						menu->ResizeToWindow(window);
						tileMap->FitToScreen(window);
					}
				}
				if (event->type == sf::Event::Closed)
					window->close();

				if (event->type == sf::Event::KeyPressed) //if Esc is pressed the application closses
					if (event->key.code == sf::Keyboard::Escape)
						window->close();
			}


			Update(deltaTime);
			Draw();

			Renderer::Instance()->Render(window);

		}
	
}


