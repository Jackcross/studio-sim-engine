#pragma once
//Creator : Jack Cross 

#include "Structs.h"
class Button
{
public:

	Button(float x, float y, float width, float hight, sf::Font* font, int fontSize, string text, sf::Color idleColor, sf::Color hoverColor, sf::Color activeColor);

	void				Draw(sf::RenderWindow* window);
	void				update(const sf::Vector2f mousePos);
	bool				isPressed();

	void				SetMenuState(MenuState state) { _Menustat = state; }//this is only for setting the menu state for each MAIN menu
	MenuState			GetState() { return _Menustat; }
	void				SetPosition(sf::Vector2f pos);
	string				GetButtonText() { return _text.getString(); }

	sf::RectangleShape* getRec() { return &_shape; }
private:

	int					_buttonState;

	MenuState			_Menustat;

	sf::RectangleShape	_shape;
	sf::Font*			_font;
	sf::Text			_text;

	sf::Color			_idleColor;
	sf::Color			_hoverColor;
	sf::Color			_PressedColor;

};

