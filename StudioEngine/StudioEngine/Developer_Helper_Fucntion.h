#pragma once
#include <vector>
#include <time.h>
#include <vector>
#include <string>
#include <iostream>

static float distanceCheck(float x1, float y1, float x2, float y2)
{
	float dx = x1 - x2;
	float dy = y1 - y2;
	return sqrt(dx * dx + dy * dy);
}

static bool Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2)
{
	if (x1 < x2 + width2 &&
		x1 + width1 > x2 &&
		y1 < y2 + hight2 &&
		y1 + hight1 > y2) {
		return true;
	}
	else
	{
		return false;
	}
}