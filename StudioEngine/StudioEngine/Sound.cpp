//Creator : Liam Davy
#include "Sound.h"

Sound::Sound(std::string name, std::string filePath, float volume, bool isLoop)
{
	_soundName = name;
	_audioSource = new AudioSource(filePath);
	_soundVolume = volume;
	_isLoop = isLoop;
}

Sound::~Sound()
{
}

std::string Sound::GetName()
{
	return _soundName;
}

void Sound::SetName(std::string name)
{
	_soundName = name;
}

void Sound::SetFilePath(std::string path)
{
	_audioSource = new AudioSource(path);
}

float Sound::GetVolume()
{
	return _soundVolume;
}

void Sound::SetVolume(float volume)
{
	_soundVolume = volume;
}

bool Sound::GetIsLooping()
{
	return _isLoop;
}

void Sound::SetIsLooping(bool isLoop)
{
	_isLoop = isLoop;
}

AudioSource* Sound::GetAudioSource()
{
	return _audioSource;
}
