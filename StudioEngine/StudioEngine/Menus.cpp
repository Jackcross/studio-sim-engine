#include "Menus.h"
#include <Windows.h>
//Creator : Jack Cross 
#define ButnIdleColour sf::Color(50, 50, 50, 255)
#define menuYbtnGap 2
#define menuXbtnGap 5

Menus::Menus(ObjectsOnScreen* ScreenObjects, TileMap* map)
{
	this->ScreenObjects = ScreenObjects;

	if (!MyFont.loadFromFile("Resources/Fonts/Roboto-MediumItalic.ttf"))
	{
		int test = 0;
		// Error...
	}
	imageMenu = new ImagesMenu(&MyFont, ScreenObjects);
	selectedMenu = new SelectedMenu(MyFont, ScreenObjects);
	mapMenu = new MapMenu(map, &MyFont);
	listMenu = new ObjectListMenu();

	state = IMAGES;
	OpenCloseState = OPEN_MENU;

	MenuLayout();
	ButtonsSetup();
	SetUpEntityList();
}

void Menus::update(sf::Vector2f mousePos, double deltaTime)//draw menus depending on which menu case state is active 
{

	SlowUpdate(deltaTime);
	mousePosition = mousePos;

	switch (OpenCloseState)
	{
	case OPEN_MENU:

		Playbtn->update(mousePos);//draw play button and stop button when play mode is active 
		Stopbtn->update(mousePos);
		if (CloseMenubtn->isPressed())
		{
			OpenCloseState = CLOSE_MENU;
			Sleep(100);
		}
		
		switch (state)
		{
		case PLAY:

			if (Stopbtn->isPressed())
			{
				state = STOP;
			}
			break;
		case STOP:
			state = IMAGES;
			break;

		case IMAGES:
			MainBtnListUpdate(mousePos);
			//Update for image selection is done in draw.
			imageMenu->Update(mousePos, event);
			CloseMenubtn->update(mousePos);
			UpdateEntityList(mousePosition);
			SaveTextbox->Update(mousePos, event);
			listMenu->Update(mousePos, ScreenObjects);
			break;

		case SELECTED:
			MainBtnListUpdate(mousePos);
			selectedMenu->Update(mousePos, event);
			CloseMenubtn->update(mousePos);
			UpdateEntityList(mousePosition);
			SaveTextbox->Update(mousePos, event);
			listMenu->Update(mousePos, ScreenObjects);
			break;
		case MAP:
			MainBtnListUpdate(mousePos);
			mapMenu->Update(mousePos, event);
			CloseMenubtn->update(mousePos);
			UpdateEntityList(mousePosition);
			SaveTextbox->Update(mousePos, event);
			listMenu->Update(mousePos, ScreenObjects);
			break;
		}

		break;

	case CLOSE_MENU:
		Playbtn->update(mousePos);//draw play button and stop button when play mode is active 
		Stopbtn->update(mousePos);		
		Loadbtn->update(mousePos);
		Savebtn->update(mousePos);
		OpenMenubtn->update(mousePos);
		UpdateEntityList(mousePosition);
		SaveTextbox->Update(mousePos, event);

		if (OpenMenubtn->isPressed())
		{
			OpenCloseState = OPEN_MENU;
			Sleep(100);

		}
		switch (state)
		{
		case PLAY:

			if (Stopbtn->isPressed())
			{
				state = STOP;
			}
			break;
		case STOP:
			state = IMAGES;
			break;

		case IMAGES:
			//Update for image selection is done in draw.
			//imageMenu->Update(mousePos, event);
			break;

		case SELECTED:
			//selectedMenu->Update(mousePos, event);
			break;
		case MAP:
			mapMenu->UpdateClosed(mousePos, event);
			break;
		}

		break;
	}


		
	if (Playbtn->isPressed() == true)
	{
		state = PLAY;
	}
	if (Savebtn->isPressed() == true && SaveTextbox->GetInputString().getString() != "")
	{
		ScreenObjects->SaveEntities(SaveTextbox->GetInputString().getString());
		EntityLoadList->AddIfNoneExistent(SaveTextbox->GetInputString().getString());
		EntityLoadList->SetSelectedOption(SaveTextbox->GetInputString().getString());
	}
	if (Loadbtn->isPressed() == true)
	{
		Sleep(100);
		selectedMenu->SetSelectedToNull();
		ScreenObjects->LoadEntitiesFromFiles(EntityLoadList->GetSelection()+ ".txt");
		SaveTextbox->SetString(EntityLoadList->GetSelection());
		listMenu->ChangeScene(ScreenObjects->GetAllEntities());
	}


}

void Menus::MainBtnListUpdate(sf::Vector2f mousePos)
{
	for (size_t i = 0; i < MainMenuBTN.size(); i++)
	{
		MainMenuBTN[i]->update(mousePosition);
		if (MainMenuBTN[i]->isPressed() == true && MainMenuBTN[i]->GetState() != NA)//Check which menu option has been selected 
		{
			state = MainMenuBTN[i]->GetState();
		}
	}
}

void Menus::MainBtnListDraw(sf::RenderWindow* gameWindow)
{

	for (size_t i = 0; i < MainMenuBTN.size(); i++)
	{
		MainMenuBTN[i]->Draw(gameWindow);
	}
}

void Menus::SetUpEntityList()
{
	vector<string> EntityList;
	EntityList = fileLoader.GetEntityFilePaths();

	for (size_t i = 0; i < EntityList.size(); i++)
	{
		string path = EntityList[i];
		path = mapMenu->RemoveTxtFromString(path);
		EntityLoadList->AddTextOption(path);
	}
	string path = EntityLoadList->GetSelection();
	SaveTextbox->SetString(path);

}

void Menus::UpdateEntityList(sf::Vector2f mousePos)
{
	EntityLoadList->Update(mousePos);
	if (EntityLoadList->CheckListChange() == true)
	{
		selectedMenu->SetSelectedToNull();
		ScreenObjects->LoadEntitiesFromFiles(EntityLoadList->GetSelection() + ".txt");
		string path = EntityLoadList->GetSelection();
		SaveTextbox->SetString(path);

	}

}

void Menus::SlowUpdate(double deltaTime)
{
	timePassed += deltaTime;

	if (timePassed >= 1.5)//update once every 1.5 seconds 
	{
		listMenu->ChangeScene(ScreenObjects->GetAllEntities());
	}

}

void Menus::ResizeToWindow(sf::RenderWindow* gameWindow)
{
	ScreenWidth = gameWindow->getSize().x;
	ScreenHeight = sf::VideoMode::getDesktopMode().height;
	sideMenu->setPosition(sf::Vector2f(ScreenWidth - 403, 30));

	Imagesbtn->SetPosition(sf::Vector2f(sideMenu->getPosition().x + 5, sideMenu->getPosition().y + menuYbtnGap));
	Selectedbtn->SetPosition(sf::Vector2f(Imagesbtn->getRec()->getPosition().x + Imagesbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap));
	Mapbtn->SetPosition(sf::Vector2f(Selectedbtn->getRec()->getPosition().x + Selectedbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap));
	OpenMenubtn->SetPosition(sf::Vector2f(Mapbtn->getRec()->getPosition().x + Mapbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap));
	CloseMenubtn->SetPosition(sf::Vector2f(Mapbtn->getRec()->getPosition().x + Mapbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap));
	SideMenuBoarder->setPosition(sf::Vector2f(sideMenu->getPosition().x + menuXbtnGap, sideMenu->getPosition().y + 40));

	mapMenu->WindowResize(gameWindow);
	imageMenu->ResizeWindow(gameWindow);
	selectedMenu->ResizeToWindow(gameWindow);
}

void Menus::Draw(sf::RenderWindow* gameWindow)
{
	mousePosition.x += gameWindow->getPosition().x;
	mousePosition.y += gameWindow->getPosition().y;

	if (event->type == sf::Event::Resized)
	{
		ResizeToWindow(gameWindow);
		Sleep(50);
	}

	switch (OpenCloseState)
	{
	case OPEN_MENU:


		switch (state)
		{
		case PLAY:
			gameWindow->draw(*TopMenu);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);

			break;
		case STOP:
			gameWindow->draw(*TopMenu);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			break;
		case IMAGES:
			gameWindow->draw(*sideMenu);
			for (size_t i = 0; i < LayoutShapes.size(); i++)
			{
				gameWindow->draw(*LayoutShapes[i]);
			}
			MainBtnListDraw(gameWindow);

			gameWindow->draw(*TopMenu);
			CloseMenubtn->Draw(gameWindow);
			imageMenu->Draw(gameWindow, LayoutShapes);
			listMenu->Draw(gameWindow);
			Savebtn->Draw(gameWindow);
			Loadbtn->Draw(gameWindow);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			EntityLoadList->Draw(gameWindow);
			SaveTextbox->Draw(gameWindow);
			break;

		case SELECTED:
			gameWindow->draw(*sideMenu);
			for (size_t i = 0; i < LayoutShapes.size(); i++)
			{
				gameWindow->draw(*LayoutShapes[i]);
			}
			MainBtnListDraw(gameWindow);
			gameWindow->draw(*TopMenu);
			listMenu->Draw(gameWindow);
			CloseMenubtn->Draw(gameWindow);
			selectedMenu->Draw(gameWindow);
			Savebtn->Draw(gameWindow);
			Loadbtn->Draw(gameWindow);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			EntityLoadList->Draw(gameWindow);
			SaveTextbox->Draw(gameWindow);
			break;

		case MAP:
			gameWindow->draw(*sideMenu);
			for (size_t i = 0; i < LayoutShapes.size(); i++)
			{
				gameWindow->draw(*LayoutShapes[i]);
			}
			MainBtnListDraw(gameWindow);

			gameWindow->draw(*TopMenu);
			CloseMenubtn->Draw(gameWindow);
			listMenu->Draw(gameWindow);
			mapMenu->Draw(gameWindow, LayoutShapes);
			Savebtn->Draw(gameWindow);
			Loadbtn->Draw(gameWindow);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			EntityLoadList->Draw(gameWindow);
			SaveTextbox->Draw(gameWindow);
			break;
		}
		break;

	case CLOSE_MENU:
	
		switch (state)
		{
		case PLAY:
			gameWindow->draw(*TopMenu);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			break;
		case STOP:
			gameWindow->draw(*TopMenu);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			break;

		case IMAGES:
			gameWindow->draw(*TopMenu);
			Savebtn->Draw(gameWindow);
			Loadbtn->Draw(gameWindow);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			OpenMenubtn->Draw(gameWindow);
			EntityLoadList->Draw(gameWindow);
			SaveTextbox->Draw(gameWindow);
			break;

		case SELECTED:
			gameWindow->draw(*TopMenu);
			Savebtn->Draw(gameWindow);
			Loadbtn->Draw(gameWindow);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			OpenMenubtn->Draw(gameWindow);
			EntityLoadList->Draw(gameWindow);
			SaveTextbox->Draw(gameWindow);
			break;

		case MAP:
			gameWindow->draw(*TopMenu);
			Savebtn->Draw(gameWindow);
			Loadbtn->Draw(gameWindow);
			Stopbtn->Draw(gameWindow);
			Playbtn->Draw(gameWindow);
			OpenMenubtn->Draw(gameWindow);
			EntityLoadList->Draw(gameWindow);
			SaveTextbox->Draw(gameWindow);
			mapMenu->DrawClosed(gameWindow, LayoutShapes);
			break;
		}
	}
}


void Menus::ButtonsSetup() //add new buttons here    //Theres an option to add a state to make it a menu button (SetMenuState)
{
	Imagesbtn = new Button(sideMenu->getPosition().x + 5, sideMenu->getPosition().y + menuYbtnGap, 68, 30, &MyFont, 17, "Images", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	Imagesbtn->SetMenuState(IMAGES);
	MainMenuBTN.push_back(Imagesbtn);

	Selectedbtn = new Button(Imagesbtn->getRec()->getPosition().x + Imagesbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y +menuYbtnGap, 80, 30, &MyFont,17, "Selected", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	Selectedbtn->SetMenuState(SELECTED);
	MainMenuBTN.push_back(Selectedbtn);	

	Mapbtn = new Button(Selectedbtn->getRec()->getPosition().x + Selectedbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap, 44, 30, &MyFont, 17, "Map", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	Mapbtn->SetMenuState(MAP);
	MainMenuBTN.push_back(Mapbtn);

	Playbtn = new Button(900, 3, 90, 25, &MyFont, 17, "Play", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	Playbtn->SetMenuState(PLAY);
	MainMenuBTN.push_back(Playbtn);

	Stopbtn = new Button(995, 3, 90, 25, &MyFont, 17, "Stop", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	MainMenuBTN.push_back(Stopbtn);

	Savebtn = new Button(210, 3, 55, 25, &MyFont, 17, "Save", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	MainMenuBTN.push_back(Savebtn);

	Loadbtn = new Button(5, 3, 55, 25, &MyFont, 17, "Load", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	MainMenuBTN.push_back(Loadbtn);

	OpenMenubtn = new Button(Mapbtn->getRec()->getPosition().x + Mapbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap, 45, 30, &MyFont, 17, "Show", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	CloseMenubtn = new Button(Mapbtn->getRec()->getPosition().x + Mapbtn->getRec()->getGlobalBounds().width + menuXbtnGap, sideMenu->getPosition().y + menuYbtnGap, 45, 30, &MyFont, 17, "Hide", ButnIdleColour, sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
}

void Menus::MenuLayout()//visual menus 
{
	ScreenWidth = sf::VideoMode::getDesktopMode().width;
	ScreenHeight = sf::VideoMode::getDesktopMode().height;

	EntityLoadList = new DropList(0, sf::Vector2f(63, 4), sf::Vector2f(100, 23));
	SaveTextbox = new TextBox(266, 4, 105, 23, &MyFont, "", false, event, "");
	SaveTextbox->SetTextAmount(14);

	sideMenu = new sf::RectangleShape();
	sideMenu->setSize(sf::Vector2f(400, 1080));
	sideMenu->setPosition(sf::Vector2f(ScreenWidth - 403, 30));
	sideMenu->setFillColor(sf::Color(30, 35, 35, 200));

	TopMenu = new sf::RectangleShape();
	TopMenu->setSize(sf::Vector2f(ScreenWidth, 30));
	TopMenu->setPosition(sf::Vector2f(0, 0));
	TopMenu->setFillColor(sf::Color(30, 35,35, 255));

	SideMenuBoarder = new sf::RectangleShape();
	SideMenuBoarder->setSize(sf::Vector2f(sideMenu->getGlobalBounds().width - 20, sideMenu->getGlobalBounds().height - 60));
	SideMenuBoarder->setPosition(sf::Vector2f(sideMenu->getPosition().x + 10, sideMenu->getPosition().y + 40));
	SideMenuBoarder->setOutlineColor(sf::Color(20, 20, 20, 200));
	SideMenuBoarder->setOutlineThickness(5);
	SideMenuBoarder->setFillColor(sf::Color(0,0,0,0));
	LayoutShapes.push_back(SideMenuBoarder);
}



