



//------------------------------------------ENGINE------------------------------------------------

// Base Class Creation :
//The engine Manager creates an instance of Menu and ObjectOnScreen, Draws and updates too. All other classes are currently created in these two. 


//OBJECTS_ON_SCREEN : 
//Contains 4 lists for objects on screen for the different Entity Types . 
//Can create an entity of each type
//Mouse clicks and drags objects already on screen
//The Menu class calls one of the create entity to create a new entity on screen

//MENU :
//The menu currently creates two other menus (image and select)
//contains the buttons for selecting the other menus and play and stop button
//The menus are passed the instance of ObjectsOnScreen to get access to the list of entites and adding or removing from that list. 

//IMAGES_MENU :
//Places all loaded sprites into the menu with spacing.
//Calls the ObjectOnScreen when dragging and droping sprite onto screen. Passing a new entity 

//SELECTED_MENU :
//Once a new object is on screen, clicking on that object will display current information in the text boxes.
//You can enter text, but this function doesnt work properly. You cannot delete text. Best use the arrows.
//Changing the type of the entity with the drop list will call the ObjectsOnscreen, adding to the entity list. 
//setting or changing the type will remove it from every other type list. The Entities list will contain every entity from every list.

//BUTTON : 
//Button(float x, float y, float width, float hight, sf::Font* font, int fontSize, string text, sf::Color idleColor, sf::Color hoverColor, sf::Color activeColor);
//Uses a sf::Rectangle for the shape and sf::Text text
//Theres an object to SetMenuState, this is only for setting the menu state for each MAIN menu

//DROPLIST :
//DropList(int amount, sf::Vector2f position, sf::Vector2f size);
//Using A vector of sf::Rectangles to create the option boxes and a vector of sf::Text for text

//TEXTBOX :
// TextBox(float posX, float posY, float width, float height, sf::Font* myFont, string text, bool ButtonBool, string ButtonText, sf::Event* event)
//Currently used in SelectedMenu to changs the values of selected entity
//An option for adding custom text exists but dosent work properly, can't delete. It became easier to using arrow buttons to change values

//UNDO_CONTROLLER :
//Due to the undo button only working for entities on screen, its is created and controlled within OBJECTSONSCREEN. 
//This contains a private struct which a new one is created each time the user makes a changed to entities on screen. 
//Each instance of the structure records what changes are made. 