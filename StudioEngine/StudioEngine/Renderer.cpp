#include "Renderer.h"
//Creator : Kamil Chmielewski
Renderer* Renderer::_instance = 0;

Renderer* Renderer::Instance()
{
	if (!_instance)
	{
		_instance = new Renderer;
	}
	return _instance;
}

Renderer::Renderer()
{

}


void Renderer::AddSprite(Sprite* sprite)
{
	sprites.push_back(sprite);
}

void Renderer::AddSprites(std::vector<Sprite*> sprites)
{
	for (Sprite* sp : sprites)
	{
		sprites.push_back(sp);
	}
}

void Renderer::AddRectangle(sf::RectangleShape* rectangle)
{
	rects.push_back(rectangle);
}

void Renderer::AddRectangles(std::vector<sf::RectangleShape*> rectangles)
{
	for (sf::RectangleShape* rect : rectangles)
	{
		rects.push_back(rect);
	}
}

void Renderer::Render(sf::RenderWindow* wd)
{
	for (sf::RectangleShape* rect : rects)
	{
		wd->draw(*rect);
	}

	for (Sprite* sp : sprites)
	{
		wd->draw(sp->GetSprite());
	}
}


