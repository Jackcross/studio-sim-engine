#pragma once
#include "Zombie.h"
class Wave
{
public:
	Wave(int slowZombies, int fastZombies, int heavyZombies);
	~Wave();
	void Update(double elaspedTime, sf::Vector2f trackPosition);
	void Draw(sf::RenderWindow* gameWindow);
	vector<Zombie*>* GetZombieList() { return &zombies; }
	vector<Entity*>* GetZombieNPCList() { return &NPCzombies; }
	int getZombiesSize() { return zombies.size(); }
private:
	vector<Zombie*> zombies;
	vector<Entity*> NPCzombies;
};

