#pragma once
//Creator : Jack Cross 

#include <vector>
#include <time.h>
#include <vector>
#include <string>
#include <iostream>
#include "SFML\Graphics.hpp"
#include "SFML\Window.hpp"
#include "SFML\System.hpp"
using namespace std;

enum WhatChanged { SCALE, POSITION, SOURCERECT, NAME, ROTATION, TYPE, GROUPMOVE };

enum Button_state { BTN_IDLE, BTN_HOVER, BTN_PRESSED };

enum Button_side {BTN_LEFT, BTN_RIGHT};

enum Button_Type { ARROWS_BTN, CHANGE_BTN };

enum Entity_Type { OBJECT_TYPE, PLAYER_TYPE, NPC_TYPE, UNDEFINED_TYPE};

enum MenuState { HOME, IMAGES, SELECTED, NA, PLAY, MAP, CLOSE_MENU, OPEN_MENU, STOP};


