#include "ImagesMenu.h"
//Creator : Jack Cross 

ImagesMenu::ImagesMenu(sf::Font* myFont, ObjectsOnScreen* ScreenObjects)
{
	this->myFont = myFont;
	this->ScreenObjects = ScreenObjects;
	SetUpReferenceShaps();
	LoadGameSprites();
	LoadImageMenuTextures();
	ButtonSetup();
	Charaters();
	CreateCharaterHighltBox();
}

void ImagesMenu::Update(sf::Vector2f mousePos, sf::Event* event)
{
	this->event = event;
	mousePosition = mousePos;
	LoadImagebtn->update(mousePos);

	if (LoadImagebtn->isPressed())
	{
		LoadGameSprites();
		Charaters();
	}
}

void ImagesMenu::Draw(sf::RenderWindow* gameWindow, vector<sf::RectangleShape*> LayoutShapes)
{
	if (mouseSprite != nullptr)
	{
		mouseSprite->SetPosition(mousePosition);
		gameWindow->draw(mouseSprite->GetSprite());
	}

	DropORResetMouseSprite(LayoutShapes);//set texture rect to zero if button released and drop sprite onto screen space

	DrawHighBoxAndSetMouseSprite(gameWindow);


	for (size_t i = 0; i < objectSprites.size(); i++)
	{
		gameWindow->draw(objectSprites[i]->GetSprite());
	}
	LoadImagebtn->Draw(gameWindow);
}



void ImagesMenu::Charaters()//load all charaters, evenly spaced out 
{
	int CalcEdgeSpace = (400 * 5) / 100;
	sf::Vector2f area(sideMenu->getGlobalBounds().width - CalcEdgeSpace, 800);

	int XscreenSpace = sideMenu->getPosition().x;
	int x = 0;
	int y = 150;
	int BiggestHeight = 0;
	for (size_t i = 0; i < objectSprites.size(); i++)
	{
		if (objectSprites[i]->GetHeight() > BiggestHeight)//get the tallest sprite on this line
		{
			BiggestHeight = objectSprites[i]->GetHeight();
		}
		if ((objectSprites[i]->GetWidth() + x) > (area.x - 5))// if theres no space left on the line, go to the next. 
		{
			y += BiggestHeight;//the next line will start with a Y difference of the largest sprite from the previous line 
			x = 0;
			BiggestHeight = 0;
		}

		objectSprites[i]->SetPosition((sideMenu->getPosition().x + CalcEdgeSpace) + x, y);//set the position 
		x += objectSprites[i]->GetWidth();// add width of current sprite to x, so that the next sprite spawns in the next position;

	}
}

void ImagesMenu::CreateCharaterHighltBox()
{
	SelectCharaterBoxes.clear();
	for (size_t i = 0; i < objectSprites.size(); i++)
	{
		sf::RectangleShape newBox;
		newBox.setPosition(objectSprites[i]->GetPostion());
		newBox.setSize(sf::Vector2f(objectSprites[i]->GetWidth(), objectSprites[i]->GetHeight()));
		newBox.setFillColor(sf::Color(20, 20, 20, 200));
		SelectCharaterBoxes.push_back(newBox);
	}
}

void ImagesMenu::CreateNewEntity(Sprite* newEntitySprite)
{
	Sprite* newImage = new Sprite();
	newImage->SetTexture(newEntitySprite->GetTexutre());
	newImage->SetSourceRect(newEntitySprite->GetSourceRect());
	newImage->SetPosition(newEntitySprite->GetPostion());
	newImage->SetScale(newEntitySprite->GetScaleValue());
	newImage->SetFilePath(newEntitySprite->GetFilePath());
	ScreenObjects->AddEntity(newImage);
}

void ImagesMenu::DropORResetMouseSprite(vector<sf::RectangleShape*> LayoutShapes)
{
	if (mouseSprite->GetSourceRect() != sf::Vector2f(0, 0))// if the user is dragging a new entity to screen space and then realeases it, create a new entity by calling createNewEntity
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu
			if (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left) && (mouseSprite->GetSourceRect() != sf::Vector2f(0, 0)) && (Collision(mouseSprite->GetPostion().x, mouseSprite->GetPostion().y, mouseSprite->GetSourceRect().x, mouseSprite->GetSourceRect().y, LayoutShapes[i]->getPosition().x, LayoutShapes[i]->getPosition().y, LayoutShapes[i]->getGlobalBounds().width, LayoutShapes[i]->getGlobalBounds().height) == false))
			{
				CreateNewEntity(mouseSprite);
			}
		}
	}

	//if user released mouse click then 
	if (mouseSprite->GetSourceRect().x > 1 && (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left)))//make sure the Sprite attached to the mouse is not showing once mouse is released 
	{
		mouseSprite->SetSourceRect(0.0f, 0.0f);
	}
}

void ImagesMenu::ResizeWindow(sf::RenderWindow* gameWindow)
{
	int ScreenWidth = gameWindow->getSize().x;

	sideMenu->setPosition(sf::Vector2f(ScreenWidth - 400, 30));
	LoadImagebtn->SetPosition(sf::Vector2f(sideMenu->getPosition().x + 17, sideMenu->getPosition().y + 47));
	int CalcEdgeSpace = (400 * 5) / 100;
	sf::Vector2f area(sideMenu->getGlobalBounds().width - CalcEdgeSpace, 800);

	int XscreenSpace = sideMenu->getPosition().x;
	int x = 0;
	int y = 150;
	int BiggestHeight = 0;
	for (size_t i = 0; i < objectSprites.size(); i++)
	{
		if (objectSprites[i]->GetHeight() > BiggestHeight)//get the tallest sprite on this line
		{
			BiggestHeight = objectSprites[i]->GetHeight();
		}
		if ((objectSprites[i]->GetWidth() + x) > (area.x - 5))// if theres no space left on the line, go to the next. 
		{
			y += BiggestHeight;//the next line will start with a Y difference of the largest sprite from the previous line 
			x = 0;
			BiggestHeight = 0;
		}

		objectSprites[i]->SetPosition((sideMenu->getPosition().x + CalcEdgeSpace) + x, y);//set the position 
		x += objectSprites[i]->GetWidth();// add width of current sprite to x, so that the next sprite spawns in the next position;
	}
	CreateCharaterHighltBox();
}

void ImagesMenu::SetUpReferenceShaps()//ONLY FOR POSITIONING PURPOSES FOR OBJECTSPRITE POSITIONS, not drawn
{
	int ScreenWidth = sf::VideoMode::getDesktopMode().width;

	sideMenu = new sf::RectangleShape();//create new rectangle shape for side menu // ONLY FOR POSITIONING PURPOSES, not drawn
	sideMenu->setSize(sf::Vector2f(400, 1080));
	sideMenu->setPosition(sf::Vector2f(ScreenWidth - 400, 30));
}

void ImagesMenu::DrawHighBoxAndSetMouseSprite(sf::RenderWindow* gameWindow)//Selecting the sprite and making copy //THE SELECTION is dependent on the position of highlight boxes
																			//(CANNOT click on any images if they're not within the bounds of the 'SelectCharaterBoxes' boxes) 
{																			//HELP: if you can't select images, make sure the SelectCharaterBoxes are positioned properly
	for (size_t i = 0; i < SelectCharaterBoxes.size(); i++)
	{
		if (SelectCharaterBoxes[i].getGlobalBounds().contains(mousePosition) && sideMenu->getGlobalBounds().contains(mousePosition) && mouseSprite->GetSourceRect() == sf::Vector2f(0,0))//check if mouse is hovering over sprites from menu
		{
			gameWindow->draw(SelectCharaterBoxes[i]);//draw highlight box
			if (event->type == sf::Event::MouseButtonPressed && (event->mouseButton.button == sf::Mouse::Left))//if the user clicks on sprite, set the mousesprite to the clicked on sprite 
			{		//set mouse texture
				sf::Texture tex = objectSprites[i]->GetTexutre();
				mouseSprite->SetTexture(tex);
				mouseSprite->SetFilePath(objectSprites[i]->GetFilePath());
				mouseSprite->SetSourceRect(objectSprites[i]->GetSourceRect());
			}
		}
	}
}

bool ImagesMenu::Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2)
{
	if (x1 < x2 + width2 &&
		x1 + width1 > x2&&
		y1 < y2 + hight2 &&
		y1 + hight1 > y2) {
		return true;
	}
	else
	{
		return false;
	}
}

void ImagesMenu::LoadGameSprites()
{
	objectSprites = fileLoader.GetSprites();
	ScreenObjects->SetStoreSprites(objectSprites);//send the store sprites to screenObjects to be used for loading in entities from file
}

void ImagesMenu::LoadImageMenuTextures()
{
	mouseSprite = new Sprite("Resources/EngineTextures/DownArrow.png", sf::Vector2f(75, 100));
	mouseSprite->SetSourceRect(sf::Vector2f(0, 0));
}

void ImagesMenu::ButtonSetup()
{
	LoadImagebtn = new Button(sideMenu->getPosition().x + 17, sideMenu->getPosition().y + 47, 110, 30, myFont, 15, "Load Image", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
}

