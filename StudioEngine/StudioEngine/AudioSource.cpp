#include "AudioSource.h"
#include <iostream>
//Creator : Liam Davy

AudioSource::AudioSource(std::string path)
{
	if (!soundBuffer.loadFromFile(path))
	{
		std::cout << "File not found";
	}
	sound.setBuffer(soundBuffer);
}

AudioSource::~AudioSource()
{
}

bool AudioSource::isPlaying()
{
	if (sound.getStatus() == 3)
	{
		return true;
	}
	return false;
}

void AudioSource::Play()
{
	if (!this->isPlaying())
	{
		sound.play();
	}
}

void AudioSource::Stop()
{
	sound.stop();
}
