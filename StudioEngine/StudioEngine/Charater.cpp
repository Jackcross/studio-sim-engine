#include "Charater.h"
//Creator : Jack Cross 

Charater::Charater()
{
	_movement = new Movement();
}

void Charater::Update()
{

}

bool Charater::DistanceToEntity(Entity* otherEntity, float distance)
{
	return _movement->DistanceCheck(otherEntity, this, distance);
}

bool Charater::DistanceCheckToPosition(sf::Vector2f pos, float distance)
{
	return _movement->DistanceCheckWithPos(this, pos, distance);
}

void Charater::SteeringUpdate(float time, Entity* current)
{
	SetPosition( _movement->UpdateSteering(time, current));
}

void Charater::FacePosition(sf::Vector2f position)
{
	const float PI = 3.14159265;

	float dot = GetImage()->GetPostion().x * position.x + GetImage()->GetPostion().y * position.y;
	float det = GetImage()->GetPostion().x * position.y - GetImage()->GetPostion().y * position.x;

	float dx = position.x - GetImage()->GetPostion().x;
	float dy = position.y - GetImage()->GetPostion().y;

	float angle = atan2f(dy, dx);
	angle = angle * (180 / PI);

	if (angle < 0)
		angle = 360 - (-angle);

	GetImage()->SetRotation(angle);
}





