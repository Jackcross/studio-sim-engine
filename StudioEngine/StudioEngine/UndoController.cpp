#include "UndoController.h"
#include <iostream>
#include <sstream>
#include <fstream>
#include <dirent.h>

UndoController::UndoController()
{
}

void UndoController::Update(vector<Entity*> entities)//Update the list of entites so Undo() has access. Check for undo Keypress 
{
	AllEntites = entities;

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LControl) && sf::Keyboard::isKeyPressed(sf::Keyboard::Z))
	{
		Sleep(200);
		Undo();
	}

}

void UndoController::Undo()//Check the undo struct for the type of Undo required. 
{
	if (undoList.size() > 0)
	{
		newUndo = undoList[undoList.size() - 1];
		for (size_t i = 0; i < AllEntites.size(); i++)
		{
			if (newUndo->Change == GROUPMOVE)
			{
				for (size_t j = 0; j < newUndo->groupMovedata.size(); j++)
				{
					if (newUndo->groupMovedata[j] == AllEntites[i])
					{
						AllEntites[i]->SetPosition(newUndo->groupMovePositionData[j]);
					}
				}
				
			}
			else if (AllEntites[i] == newUndo->entityHold)
			{
				if (newUndo->Change == NAME)
				{
					AllEntites[i]->SetName(newUndo->stringData);
				}
				else if (newUndo->Change == POSITION)
				{
					AllEntites[i]->SetPosition(newUndo->vecData);
				}
				else if (newUndo->Change == SOURCERECT)
				{
					AllEntites[i]->GetImage()->SetSourceRect(newUndo->vecData);
				}
				else if (newUndo->Change == SCALE)
				{
					AllEntites[i]->GetImage()->SetScale(newUndo->floatData);
				}
				else if (newUndo->Change == ROTATION)
				{
					AllEntites[i]->GetImage()->SetRotation(newUndo->floatData);
				}

			}
		}
		
		undoList.pop_back();
	}
}

void UndoController::UndoSetChange(WhatChanged change, void* data, Entity* currentEntity)//When a change is made, the controlling class for that change will call this method to log the change
{
	UndoStruct* newUndo = new UndoStruct(change, data, currentEntity);
	undoList.push_back(newUndo);
}
