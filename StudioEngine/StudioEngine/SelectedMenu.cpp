#include "SelectedMenu.h"
#include <iomanip>
#include <sstream>
//Creator : Jack Cross 

// The select Menu is used to change the size and position of the object 
SelectedMenu::SelectedMenu(sf::Font font, ObjectsOnScreen* objectOnscreen)// You need an instance of ObjectOnScreen to access all values of each entity to be displayed in textboxes
{
	SetupReferenceShaps();

	displaySprite = new Sprite();

	float sideMenuMiddle = sideMenu->getPosition().x + (sideMenu->getGlobalBounds().width / 2);
	displaySprite->SetPosition(sf::Vector2f(sideMenuMiddle, 180));//move to position
	this->objectOnscreen = objectOnscreen;
	MyFont = font;
	CheckSelected = false;
	SetupTextBox();
	ListSetup();
}

void SelectedMenu::Update(sf::Vector2f mousePos, sf::Event* event)//update text boxes and droplists 
{
	this->event = event;
	for (size_t i = 0; i < SelectedTextboxs.size(); i++)
	{
		SelectedTextboxs[i]->Update(mousePos, event);
	}
	for (size_t i = 0; i < DropLists.size(); i++)
	{
		DropLists[i]->Update(mousePos);
	}
	SpriteSelected();
}

void SelectedMenu::Draw(sf::RenderWindow* gameWindow)
{
	for (size_t i = 0; i < SelectedTextboxs.size(); i++)
	{
		SelectedTextboxs[i]->Draw(gameWindow);
	}
	for (size_t i = 0; i < Textboxs.size(); i++)
	{
		gameWindow->draw(*Textboxs[i]);
	}
	for (size_t i = 0; i < DropLists.size(); i++)
	{
		DropLists[i]->Draw(gameWindow);
	}
	gameWindow->draw(displaySprite->GetSprite());//display sprite selected 
}

void SelectedMenu::SetupTextBox()
{
	int BTNPositionScaleX = sideMenu->getPosition().x + 150;///1520 position of side menu
	int BTNPositionStartY = 30 + 300;//30 y position of side menu

	//text lables for text boxes
	SetPositionText = new sf::Text();
	SetPositionText->setCharacterSize(19);
	SetPositionText->setFont(MyFont);
	SetPositionText->setFillColor(sf::Color(0, 0, 0, 255));
	SetPositionText->setString("Position:");
	SetPositionText->setPosition(sf::Vector2f(BTNPositionScaleX - 84, BTNPositionStartY));
	Textboxs.push_back(SetPositionText);

	SetScaleText = new sf::Text();
	SetScaleText->setCharacterSize(19);
	SetScaleText->setFont(MyFont);
	SetScaleText->setFillColor(sf::Color(0, 0, 0, 255));
	SetScaleText->setPosition(sf::Vector2f(BTNPositionScaleX - 59, BTNPositionStartY + 30));
	SetScaleText->setString("Scale:");
	Textboxs.push_back(SetScaleText);

	SetSourceRectText = new sf::Text();
	SetSourceRectText->setCharacterSize(19);
	SetSourceRectText->setFont(MyFont);
	SetSourceRectText->setFillColor(sf::Color(0, 0, 0, 255));
	SetSourceRectText->setPosition(sf::Vector2f(BTNPositionScaleX - 110, BTNPositionStartY + 60));
	SetSourceRectText->setString("SourceRect:");
	Textboxs.push_back(SetSourceRectText);

	SetNameText = new sf::Text();
	SetNameText->setCharacterSize(19);
	SetNameText->setFont(MyFont);
	SetNameText->setFillColor(sf::Color(0, 0, 0, 255));
	SetNameText->setPosition(sf::Vector2f(BTNPositionScaleX - 61, BTNPositionStartY + 90));
	SetNameText->setString("Name:");
	Textboxs.push_back(SetNameText);

	SetRotationText = new sf::Text();
	SetRotationText->setCharacterSize(19);
	SetRotationText->setFont(MyFont);
	SetRotationText->setFillColor(sf::Color(0, 0, 0, 255));
	SetRotationText->setPosition(sf::Vector2f(BTNPositionScaleX - 83, BTNPositionStartY + 120));
	SetRotationText->setString("Rotation:");
	Textboxs.push_back(SetRotationText);

	//text boxes (input not working properly)
	SetPositionBOXX = new TextBox(BTNPositionScaleX, BTNPositionStartY, 55, 22, &MyFont, "n/a", true, event, "");
	SelectedTextboxs.push_back(SetPositionBOXX);

	SetPositionBOXY = new TextBox(BTNPositionScaleX + SetPositionBOXX->GetWidthWithButtons() + 5, BTNPositionStartY, 55, 22, &MyFont, "n/a", true, event, "");
	SelectedTextboxs.push_back(SetPositionBOXY);

	SetScaleBOX = new TextBox(BTNPositionScaleX, BTNPositionStartY + 30, 100, 22, &MyFont, "n/a", true, event, "");
	SetScaleBOX->SetAddAmount(0.05);//the buttons add or more scale by an amount of 0.5 each click 
	SelectedTextboxs.push_back(SetScaleBOX);

	SetSourceRectBOXX = new TextBox(BTNPositionScaleX, BTNPositionStartY + 60, 55, 22, &MyFont, "n/a", true, event, "");
	SelectedTextboxs.push_back(SetSourceRectBOXX);

	SetSourceRectBOXY = new TextBox(BTNPositionScaleX + SetSourceRectBOXX->GetWidthWithButtons() + 5, BTNPositionStartY + 60, 55, 22, &MyFont, "n/a", true, event, "");
	SelectedTextboxs.push_back(SetSourceRectBOXY);

	SetNameBOX = new TextBox(BTNPositionScaleX, BTNPositionStartY + 90, 100, 22, &MyFont, "n/a", false, event, "");
	SelectedTextboxs.push_back(SetNameBOX);

	SetRotationBOX = new TextBox(BTNPositionScaleX, BTNPositionStartY + 120, 60, 22, &MyFont, "n/a", true, event, "");
	SetRotationBOX->SetAddAmount(2);
	SelectedTextboxs.push_back(SetRotationBOX);

}

void SelectedMenu::SetupReferenceShaps()
{
	int ScreenWidth = sf::VideoMode::getDesktopMode().width;
	int ScreenHeight = sf::VideoMode::getDesktopMode().height;

	sideMenu = new sf::RectangleShape();//create new rectangle shape for side menu // ONLY FOR POSITIONING PURPOSES, not drawn
	sideMenu->setSize(sf::Vector2f(400, 1080));
	sideMenu->setPosition(sf::Vector2f(ScreenWidth - 400, 30));
}

void SelectedMenu::SpriteSelected()
{

	if (objectOnscreen->returnEntitySelected() != selectedEntity)//check with ObjectOnScreen class if selected object has been changed
	{
		selectedEntity = objectOnscreen->returnEntitySelected();

		if (selectedEntity != nullptr)
		{
			UpdateMenuOptionsWithNewObject();//update dropdown list and scale box 

			displaySprite->SetTexture(selectedEntity->GetImage()->GetTexutre());
			displaySprite->SetSourceRect(selectedEntity->GetImage()->GetSourceRect());//Create Sprite to display selected sprite at top of menu
			CheckSelectedEnitySize();

		}
	}

	//Updating the entity values if the boxes have been changed 
	if (selectedEntity != nullptr)									
	{
		if (SetPositionBOXX->CheckSelected() == true)//checks if the buttons have been pressed
		{
			selectedEntity->GetImage()->SetPositionX(SetPositionBOXX->GetInputFloat());//entity position x 

			if(SetPositionBOXX->GetArrowPress() == BTN_LEFT)//CHeck with arrow was pressed
			objectOnscreen->UndoControl->UndoSetChange(POSITION, &sf::Vector2f(SetPositionBOXX->GetInputFloat() + 0.5, selectedEntity->GetPosition().y), selectedEntity);//Pass the change to the UndoClass to be logged 
			else if(SetPositionBOXX->GetArrowPress() == BTN_RIGHT)
			objectOnscreen->UndoControl->UndoSetChange(POSITION, &sf::Vector2f(SetPositionBOXX->GetInputFloat() - 0.5, selectedEntity->GetPosition().y), selectedEntity);//Pass the change to the UndoClass to be logged 
		}

		if (SetPositionBOXY->CheckSelected() == true)//checks if the buttons have been pressed
		{
			selectedEntity->GetImage()->SetPositionY(SetPositionBOXY->GetInputFloat());//entity position y 

			if (SetPositionBOXY->GetArrowPress() == BTN_LEFT)
				objectOnscreen->UndoControl->UndoSetChange(POSITION, &sf::Vector2f(selectedEntity->GetPosition().x, SetPositionBOXY->GetInputFloat() + 0.5), selectedEntity);//Pass the change to the UndoClass to be logged 
			else if (SetPositionBOXY->GetArrowPress() == BTN_RIGHT)
				objectOnscreen->UndoControl->UndoSetChange(POSITION, &sf::Vector2f(selectedEntity->GetPosition().x, SetPositionBOXY->GetInputFloat() - 0.5), selectedEntity);//Pass the change to the UndoClass to be logged 		
		}

		if (SetSourceRectBOXX->CheckSelected() == true)//checks if the buttons have been pressed
		{
			selectedEntity->GetImage()->SetSourceRectX(SetSourceRectBOXX->GetInputFloat());//entity SourceRect x 

			if (SetSourceRectBOXX->GetArrowPress() == BTN_LEFT)
				objectOnscreen->UndoControl->UndoSetChange(SOURCERECT, &sf::Vector2f(SetSourceRectBOXX->GetInputFloat() + 0.5, selectedEntity->GetImage()->GetSourceRect().y), selectedEntity);//Pass the change to the UndoClass to be logged 
			else if (SetSourceRectBOXX->GetArrowPress() == BTN_RIGHT)
				objectOnscreen->UndoControl->UndoSetChange(SOURCERECT, &sf::Vector2f(SetSourceRectBOXX->GetInputFloat() - 0.5, selectedEntity->GetImage()->GetSourceRect().y), selectedEntity);//Pass the change to the UndoClass to be logged 
		}

		if (SetSourceRectBOXY->CheckSelected() == true)//checks if the buttons have been pressed
		{
			selectedEntity->GetImage()->SetSourceRectY(SetSourceRectBOXY->GetInputFloat());//entity SourceRect y 

			if (SetSourceRectBOXY->GetArrowPress() == BTN_LEFT)
				objectOnscreen->UndoControl->UndoSetChange(SOURCERECT, &sf::Vector2f(selectedEntity->GetImage()->GetSourceRect().x ,SetSourceRectBOXY->GetInputFloat() + 0.5), selectedEntity);//Pass the change to the UndoClass to be logged 
			else if (SetSourceRectBOXY->GetArrowPress() == BTN_RIGHT)
				objectOnscreen->UndoControl->UndoSetChange(SOURCERECT, &sf::Vector2f(selectedEntity->GetImage()->GetSourceRect().x, SetSourceRectBOXY->GetInputFloat() - 0.5), selectedEntity);//Pass the change to the UndoClass to be logged 
		}

		if (SetScaleBOX->CheckSelected() == true)//checks if the buttons have been pressed
		{
			selectedEntity->GetImage()->SetScale(SetScaleBOX->GetInputFloat());//entity scale  

			float ScaleBoxFloat = SetScaleBOX->GetInputFloat();
			if (SetScaleBOX->GetArrowPress() == BTN_LEFT)	
				objectOnscreen->UndoControl->UndoSetChange(SCALE, &ScaleBoxFloat - (int)0.5, selectedEntity);//Pass the change to the UndoClass to be logged 
			else if (SetScaleBOX->GetArrowPress() == BTN_RIGHT)
				objectOnscreen->UndoControl->UndoSetChange(SCALE, &ScaleBoxFloat - (int)0.5, selectedEntity);//Pass the change to the UndoClass to be logged 
		}
		
		//if (SetNameBOX->isTextBoxSelected() == true)
		//{
		//	if (CheckSelected == false)
		//	{
		//		objectOnscreen->UndoControl->UndoSetChange(NAME, &(string)SetNameBOX->GetInputString().getString(), selectedEntity);//Pass the change to the UndoClass to be logged 
		//		CheckSelected = true;
		//	}
		//}

		if (SetNameBOX->CheckSelected() == true)//checks if the buttons have been pressed
			selectedEntity->SetName(SetNameBOX->GetInputString().getString());//entity scale 


		if (SetRotationBOX->CheckSelected() == true)
		{
			selectedEntity->GetImage()->SetRotation(SetRotationBOX->GetInputFloat());

			float RotationBoxFloat = SetRotationBOX->GetInputFloat();
			float test = RotationBoxFloat - (int)1;

			if (SetRotationBOX->GetArrowPress() == BTN_LEFT)
				objectOnscreen->UndoControl->UndoSetChange(ROTATION, &RotationBoxFloat + (int)1.0f, selectedEntity);//Pass the change to the UndoClass to be logged 
			else if (SetRotationBOX->GetArrowPress() == BTN_RIGHT)
				objectOnscreen->UndoControl->UndoSetChange(ROTATION, &RotationBoxFloat - (int)1, selectedEntity);//Pass the change to the UndoClass to be logged 
		}



		SetPositionBOXX->SetFloat(selectedEntity->GetPosition().x);//keep the boxes updated with the latest information from the selected entity 
		SetPositionBOXY->SetFloat(selectedEntity->GetPosition().y);
		SetSourceRectBOXY->SetFloat(selectedEntity->GetImage()->GetSourceRect().y);
		SetSourceRectBOXX->SetFloat(selectedEntity->GetImage()->GetSourceRect().x);
		SetRotationBOX->SetFloat(selectedEntity->GetImage()->GetRotation());;
	}

	if (selectedEntity != nullptr)
	{
		if (TypeSelectList->CheckListChange() == true)///check if user has interacted with drop down list //change the Entity type depending on selected option from the drop down list
		{
			string test = TypeSelectList->GetSelection();
			if (TypeSelectList->GetSelection() == "Player" && selectedEntity->GetType() != PLAYER_TYPE)
			{
				objectOnscreen->CreateNewPlayer(selectedEntity->GetImage());
				objectOnscreen->RemoveEntity(selectedEntity);
			}
			else if (TypeSelectList->GetSelection() == "NPC" && selectedEntity->GetType() != NPC_TYPE)
			{
				objectOnscreen->CreateNewNPC(selectedEntity->GetImage());
				objectOnscreen->RemoveEntity(selectedEntity);
			}
			else if (TypeSelectList->GetSelection() == "Object" && selectedEntity->GetType() != OBJECT_TYPE)
			{
				objectOnscreen->CreateNewObject(selectedEntity->GetImage());
				objectOnscreen->RemoveEntity(selectedEntity);
			}
			else if (TypeSelectList->GetSelection() == "n/a" && selectedEntity->GetType() != NA)
			{
				objectOnscreen->CreateNewEntity(selectedEntity->GetImage());
				objectOnscreen->RemoveEntity(selectedEntity);
			}
		}
	}
}

void SelectedMenu::UpdateMenuOptionsWithNewObject()//update the dropdown list 
{

	SetScaleBOX->SetFloat(selectedEntity->GetImage()->GetScaleValue());// update scale box with the latest information from selected entity 
	SetNameBOX->SetString(selectedEntity->GetName());

	//enum Entity_Type { OBJECT_TYPE, PLAYER_TYPE, NPC_TYPE, UNDEFINED_TYPE};

	if (selectedEntity->GetType() == UNDEFINED_TYPE)
	{
		TypeSelectList->SetSelectedOption("n/a");
	}
	else if (selectedEntity->GetType() == PLAYER_TYPE)
	{
		TypeSelectList->SetSelectedOption("Player");
	}
	else if (selectedEntity->GetType() == NPC_TYPE)
	{
		TypeSelectList->SetSelectedOption("NPC");
	}
	else if (selectedEntity->GetType() == OBJECT_TYPE)
	{
		TypeSelectList->SetSelectedOption("Object");
	}
	
}

void SelectedMenu::ListSetup()//create the dropdown list 
{
	int BTNPositionScaleX = sideMenu->getPosition().x + 150;///1520 position of side menu

	TypeSelectList = new DropList(4, sf::Vector2f(BTNPositionScaleX, 480), sf::Vector2f(100, 18));
	TypeSelectList->SetTextAt(0, "Player");
	TypeSelectList->SetTextAt(1, "NPC");
	TypeSelectList->SetTextAt(2, "Object");
	TypeSelectList->SetTextAt(3, "n/a");

	DropLists.push_back(TypeSelectList);
}

void SelectedMenu::CheckSelectedEnitySize()
{
	displaySprite->SetScale(1.0);
	float GetSideMenuX = sideMenu->getPosition().x + sideMenu->getGlobalBounds().width;
	float GetSpriteX = displaySprite->GetPostion().x + selectedEntity->GetImage()->GetWidth();
	while (GetSpriteX >= GetSideMenuX)
	{
		displaySprite->SetScale(displaySprite->GetScaleValue() - 0.1);
		GetSpriteX = displaySprite->GetPostion().x + displaySprite->GetWidth();
	}
}

void SelectedMenu::ResizeToWindow(sf::RenderWindow* gameWindow)
{
	sideMenu->setPosition(sf::Vector2f(gameWindow->getSize().x - 400, 30));

	int BTNPositionScaleX = sideMenu->getPosition().x + 150;///1520 position of side menu
	int BTNPositionStartY = 30 + 300;//30 y position of side menu

	SetPositionText->setPosition(sf::Vector2f(BTNPositionScaleX - 84, BTNPositionStartY));
	SetScaleText->setPosition(sf::Vector2f(BTNPositionScaleX - 59, BTNPositionStartY + 30));
	SetSourceRectText->setPosition(sf::Vector2f(BTNPositionScaleX - 110, BTNPositionStartY + 60));
	SetNameText->setPosition(sf::Vector2f(BTNPositionScaleX - 61, BTNPositionStartY + 90));
	SetRotationText->setPosition(sf::Vector2f(BTNPositionScaleX - 83, BTNPositionStartY + 120));
	
	
	SetPositionBOXX->SetPosition(sf::Vector2f(BTNPositionScaleX, BTNPositionStartY));
	SetPositionBOXY->SetPosition(sf::Vector2f(BTNPositionScaleX + SetPositionBOXX->GetWidthWithButtons() + 5, BTNPositionStartY));
	SetScaleBOX->SetPosition(sf::Vector2f(BTNPositionScaleX, BTNPositionStartY + 30));
	SetSourceRectBOXX->SetPosition(sf::Vector2f(BTNPositionScaleX, BTNPositionStartY + 60));
	SetSourceRectBOXY->SetPosition(sf::Vector2f(BTNPositionScaleX + SetSourceRectBOXX->GetWidthWithButtons() + 5, BTNPositionStartY + 60));
	SetNameBOX->SetPosition(sf::Vector2f(BTNPositionScaleX, BTNPositionStartY + 90));
	SetRotationBOX->SetPosition(sf::Vector2f(BTNPositionScaleX, BTNPositionStartY + 120));
	TypeSelectList->SetPosition(sf::Vector2f(sideMenu->getPosition().x + 150, 480));
	float sideMenuMiddle = sideMenu->getPosition().x + (sideMenu->getGlobalBounds().width / 2);
	displaySprite->SetPosition(sf::Vector2f(sideMenuMiddle, 180));//move to position

}


string SelectedMenu::FloatToString(float num)
{
	stringstream stream;
	stream << std::fixed << std::setprecision(1) << num;
	std::string s = stream.str();
	return s;
}

