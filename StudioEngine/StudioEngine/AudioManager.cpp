//Creator : Liam Davy
#include "AudioManager.h"

AudioManager::AudioManager()
{
}

AudioManager::~AudioManager()
{
}

void AudioManager::AddSoundToList(std::string name, std::string filePath, float volume, bool isLoop)
{
	Sound* sound = new Sound(name, filePath, volume, isLoop);
	_gameSounds.push_back(sound);
}

void AudioManager::RemoveSoundFromList(std::string name)
{
	for (Sound* sound : _gameSounds)
	{
		if (name == sound->GetName())
		{
			_gameSounds.remove(sound);
		}
	}
}

void AudioManager::PlaySounds(std::string soundName)
{
	for (Sound* sound : _gameSounds)
	{
		if (soundName == sound->GetName())
		{
			sound->GetAudioSource()->Play();
		}
	}
}

void AudioManager::StopSound(std::string soundName)
{
	for (Sound* sound : _gameSounds)
	{
		if (soundName == sound->GetName())
		{
			sound->GetAudioSource()->Stop();
		}
	}
}

bool AudioManager::CheckIfPlaying(std::string soundName)
{
	for (Sound* sound : _gameSounds)
	{
		if (soundName == sound->GetName())
		{
			if (sound->GetAudioSource()->isPlaying())
			{
				return true;
			}
			else
			{
				return false;
			}
		}
	}
	return false;
}
