#pragma once
//Creator : Kamil Chmielewski
#include "Sprite.h"
#include "SFML/Graphics/RectangleShape.hpp"
#include "SFML/Graphics/RenderWindow.hpp"
#include <vector>

class Renderer
{
private:

	std::vector<Sprite*> sprites;
	std::vector<sf::RectangleShape*> rects;
	static Renderer* _instance;
	Renderer();
public:
	static Renderer* Instance();

	void AddSprite(Sprite* sprite);
	void AddSprites(std::vector<Sprite*>sprites);
	void AddRectangle(sf::RectangleShape* rectangle);
	void AddRectangles(std::vector<sf::RectangleShape*> rectangles);

	void Render(sf::RenderWindow* wd);
};

/*In Sprite add std::String for name, so use can search for a certain name and apply an effect*/

//ListOfObjects
//AddObjects
//RemoveOjbects
//drawObjects