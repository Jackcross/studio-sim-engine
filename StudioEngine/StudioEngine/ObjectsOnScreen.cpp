#include "ObjectsOnScreen.h"

static float dist(sf::Vector2f a, sf::Vector2f b)
{
	float ySeparation = a.y - b.y;
	float xSeparation = a.x - b.x;

	return sqrt(ySeparation * ySeparation + xSeparation * xSeparation);
}
ObjectsOnScreen::ObjectsOnScreen()//  THIS CLASS ONLY HANDLES THE OBJECTS WHILE IN THE EDITOR, THE GAMEMAIN CLASS IS HANDED CONTROL ONCE GAME START
{
	Sleep(70);

	if (!MyFont.loadFromFile("Resources/Fonts/Roboto-MediumItalic.ttf"))
	{
		int test = 0;
		// Error...
	}
	EntitySelected = nullptr;
	SpriteSelected = nullptr;
	UndoControl = new UndoController();
	mouseState = IDLE;
	copyEntity = nullptr;
	HoldBeforeCopyEntity = nullptr;
	rightClickOpenBool = false;
	isMouseMoving = true;
	copybtn = new Button(0, 0, 90, 25, &MyFont, 17, "Copy", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	pastebtn = new Button(0, 0, 90, 25, &MyFont, 17, "Paste", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	deletebtn = new Button(0, 0, 90, 25, &MyFont, 17, "Delete", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	//SceneNumber = 0;
	groupBoxState = 0;
}

void ObjectsOnScreen::Draw(sf::RenderWindow* gameWindow)//The entites are only drawn here during editing, when the game starts, drawing of entities is passed to the Game
{
	for (size_t i = 0; i < AllEntites.size(); i++)
	{
		gameWindow->draw(AllEntites[i]->GetImage()->GetSprite());
	}
	DrawRightClick(gameWindow);

	for (size_t i = 0; i < groupHightlight.size(); i++)
	{
		gameWindow->draw(groupHightlight[i]);
	}
}

void ObjectsOnScreen::Update(sf::Vector2f mousePos, sf::Event* event, float Time)
{
	mousePosition = mousePos;

	UpdateEntityMoveWithMouse( event);

	GroupSelection(mousePosition, event);//used to highlight and move multiple entities //COPY PASTE NOT WORKING WITH MULTIPLE OBJECTS

	UndoControl->Update(AllEntites);

}

void ObjectsOnScreen::UpdateEntityMoveWithMouse( sf::Event* event)///Drag objects on screen using the mouse
{
	if (AllEntites.size() > 0)
	{
		switch (mouseState)
		{
		case IDLE:
				for (size_t i = 0; i < AllEntites.size(); i++)
				{
					UpdateRightClick(AllEntites[i], event);//update the check for right click 
					if (AllEntites.size() == 0) break;
					if (AllEntites[i]->GetImage()->GetSprite().getGlobalBounds().contains(mousePosition) && (event->type == sf::Event::MouseButtonPressed && (event->mouseButton.button == sf::Mouse::Left)))//check mouse is over entity sprite
					{
						mouseHoldSprite = AllEntites[i]->GetImage();//update selected entites and sprite;
						mousePosOnImageX = mouseHoldSprite->GetPostion().x - mousePosition.x;//get mouse Position on image 
						mousePosOnImageY = mouseHoldSprite->GetPostion().y - mousePosition.y;
						EntitySelected = AllEntites[i];//Change to the selected sprite on the selected menu 
						SpriteSelected = AllEntites[i]->GetImage();
						entityUndoPositionHold = AllEntites[i]->GetPosition();
						undoEntityNum = i;
						mouseState = MOUSEHOLD;
					}
				}
			break;
		case MOUSEHOLD:
		
			mouseHoldSprite->SetPosition(sf::Vector2f( mousePosition.x + mousePosOnImageX, mousePosition.y + mousePosOnImageY));//constantly match the position of the entity with the mouse position while left click is held

			if (mouseHoldSprite->GetPostion() != entityUndoPositionHold && isMouseMoving == true)//Check if the mouse is moving with the Entity, if so, pass entity to undo function 
			{
				if (groupHightlight.size() <= 0)//Only call Undo if the group size is <=0, The GroupSelection method has its own call to undo for special group-undo calls to Undo 
				{
					UndoControl->UndoSetChange(POSITION, &AllEntites[undoEntityNum]->GetPosition(), AllEntites[undoEntityNum]);
					isMouseMoving = false;
				}
			}

			if (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left))//if the mouse is released then drop the entity there.
			{
				mouseHoldSprite = nullptr;
				mouseState = IDLE;
				isMouseMoving = true;
			}
			break;
		}
	}
}

void ObjectsOnScreen::SaveEntities(string path)
{
	fileLoader.OutputEntities(path, AllEntites, StoreSprites); 
	fileLoader.MakeHeaderFile(AllEntites);
}

void ObjectsOnScreen::UpdateRightClick( Entity* entity, sf::Event* event)//Controls the right click menu for delete,copy and paste
{

	if (rightClickOpenBool == true || sf::Keyboard::isKeyPressed(sf::Keyboard::V))
	{
		copybtn->update(mousePosition);
		pastebtn->update(mousePosition);
		deletebtn->update(mousePosition);


		if (copybtn->isPressed() && entity->GetImage()->GetSprite().getGlobalBounds().contains(mousePosition))
		{
			copyEntity = entity;
			rightClickOpenBool = false;
		}
		
		if (pastebtn->isPressed() && copyEntity != nullptr || (sf::Keyboard::isKeyPressed(sf::Keyboard::V) && sf::Keyboard::isKeyPressed(sf::Keyboard::LControl)) && copyEntity != nullptr)
		{
			Sleep(70);
			CreateEntityBasedOnType(copyEntity);
			rightClickOpenBool = false;
		}

		if (deletebtn->isPressed() )
		{
			RemoveEntity(HoldBeforeCopyEntity);
			rightClickOpenBool = false;
		}		
	}

	if ((event->type == sf::Event::MouseButtonPressed && (event->mouseButton.button == sf::Mouse::Right)))//check mouse is over entity sprite
	{
		rightClickOpenBool = true;
		copybtn->SetPosition(mousePosition);
		pastebtn->SetPosition(sf::Vector2f(copybtn->getRec()->getPosition().x, copybtn->getRec()->getPosition().y + copybtn->getRec()->getGlobalBounds().height));
		deletebtn->SetPosition(sf::Vector2f(pastebtn->getRec()->getPosition().x, pastebtn->getRec()->getPosition().y + pastebtn->getRec()->getGlobalBounds().height));

	}

	if ((event->type == sf::Event::MouseButtonPressed && (event->mouseButton.button == sf::Mouse::Right)) && entity->GetImage()->GetSprite().getGlobalBounds().contains(mousePosition))//check mouse is over entity sprite
	{
		HoldBeforeCopyEntity = entity;
		rightClickOpenBool = true;
		copybtn->SetPosition(mousePosition);
		pastebtn->SetPosition(sf::Vector2f(copybtn->getRec()->getPosition().x, copybtn->getRec()->getPosition().y + copybtn->getRec()->getGlobalBounds().height));
		deletebtn->SetPosition(sf::Vector2f(pastebtn->getRec()->getPosition().x, pastebtn->getRec()->getPosition().y + pastebtn->getRec()->getGlobalBounds().height));

	}

	if(pastebtn->getRec()->getGlobalBounds().contains(mousePosition) == false && copybtn->getRec()->getGlobalBounds().contains(mousePosition) == false && deletebtn->getRec()->getGlobalBounds().contains(mousePosition) == false && sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		rightClickOpenBool = false;
	}
}

void ObjectsOnScreen::DrawRightClick(sf::RenderWindow* gameWindow)
{
	if (rightClickOpenBool)
	{
		copybtn->Draw(gameWindow);
		pastebtn->Draw(gameWindow);
		deletebtn->Draw(gameWindow);
	}
}

void ObjectsOnScreen::AddHighlightBoxToEntity(string name) ///This method is only used by the ObjectListMenu::Update function
{
	for (size_t i = 0; i < AllEntites.size(); i++)
	{
		if (name == AllEntites[i]->GetName())
		{
			sf::RectangleShape newRect;
			newRect.setSize(sf::Vector2f(AllEntites[i]->GetImage()->GetWidth() + 10, AllEntites[i]->GetImage()->GetHeight() + 10));
			newRect.setPosition(sf::Vector2f(AllEntites[i]->GetImage()->GetSprite().getGlobalBounds().left - 5, AllEntites[i]->GetImage()->GetSprite().getGlobalBounds().top - 5));
			newRect.setOutlineColor(sf::Color::Yellow);
			newRect.setOutlineThickness(3);
			newRect.setFillColor(sf::Color(0, 0, 0, 0));
			groupHightlight.push_back(newRect);
			highLightedEntities.push_back(AllEntites[i]);
			Sleep(100);
		}
	}


}

void ObjectsOnScreen::GroupSelection(sf::Vector2f mousePos, sf::Event* event)//BUG the user must re-press shift each time highlighting an entity
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::LShift))//Create yellow highlight box around selected entity
	{
		for (size_t i = 0; i < AllEntites.size(); i++)
		{
			if (AllEntites[i]->GetImage()->GetSprite().getGlobalBounds().contains(mousePosition) && (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left)))//check mouse is over entity sprite
			{
				sf::RectangleShape newRect;
				newRect.setSize(sf::Vector2f(AllEntites[i]->GetImage()->GetWidth() + 10, AllEntites[i]->GetImage()->GetHeight() + 10));
				newRect.setPosition(sf::Vector2f(AllEntites[i]->GetImage()->GetSprite().getGlobalBounds().left - 5, AllEntites[i]->GetImage()->GetSprite().getGlobalBounds().top - 5));
				newRect.setOutlineColor(sf::Color::Yellow);
				newRect.setOutlineThickness(3);
				newRect.setFillColor(sf::Color(0, 0, 0, 0));
				groupHightlight.push_back(newRect);
				highLightedEntities.push_back(AllEntites[i]);
				Sleep(100);
			}
		}
	}
	if (groupHightlight.size() > 0)
	{
		switch (groupBoxState)//the first case if for check which highlighted entity has been clicked 
		{					  // second case is for the movement calculations 
		case 0:
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left) && sf::Keyboard::isKeyPressed(sf::Keyboard::LShift) == false)
			{
				bool delelteList = true;//if clicking anywhere besides a highlighted entity, all high light boxes will be deleted

				for (size_t i = 0; i < groupHightlight.size(); i++)
				{
					
					if (groupHightlight[i].getGlobalBounds().contains(mousePosition) && (event->type == sf::Event::MouseButtonPressed && (event->mouseButton.button == sf::Mouse::Left)))
					{//clicking on a highlighted entity will change the switch case to one that moves all highlighted entities with mouse
						groupNumber = i;
						groupBoxState = 1;
						UndoControl->UndoSetChange(GROUPMOVE, &highLightedEntities, nullptr);//Pass the highlighted Entities to the UndoClass 
						delelteList = false;// a highlighted entity has been selected, so they dont need to deleted
					}
				}
				if (delelteList == true)
				{//clear all lists related to highlighting 
					groupHightlight.erase(groupHightlight.begin(), groupHightlight.end());
					highLightedEntities.erase(highLightedEntities.begin(), highLightedEntities.end());
				}
			}
		}
			break;
		case 1:
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				float incurmentX = 0;//incurment is the amount we need to mvoe each entity 
				float incurmentY = 0;
				
				//we move the highlighted boxes depending on the position of the entity, which is moved by the mouse.
				//the incurment calculated how are the entity the mouse is moving then applies that movement to everyother highlighted entities
				float entityPosX = highLightedEntities[groupNumber]->GetImage()->GetSprite().getGlobalBounds().left;
				float entityPosY = highLightedEntities[groupNumber]->GetImage()->GetSprite().getGlobalBounds().top;
				sf::Vector2f newPos(entityPosX, entityPosY);

				float mousePosOnRectX = groupHightlight[groupNumber].getPosition().x - entityPosX;//get rect Position on image 
				float mousePosOnRectY = groupHightlight[groupNumber].getPosition().y - entityPosY;

				if (groupHightlight[groupNumber].getPosition().x > entityPosX)
				{
					incurmentX += dist(groupHightlight[groupNumber].getPosition(), newPos);
				}
				else if (groupHightlight[groupNumber].getPosition().x < entityPosX)
				{
					incurmentX -= dist(groupHightlight[groupNumber].getPosition(), newPos);
				}

				if (groupHightlight[groupNumber].getPosition().y > entityPosY)
				{
					incurmentY += dist(groupHightlight[groupNumber].getPosition(), newPos);
				}
				else if (groupHightlight[groupNumber].getPosition().y < entityPosY)
				{
					incurmentY -= dist(groupHightlight[groupNumber].getPosition(), newPos);
				}

				for (size_t i = 0; i < groupHightlight.size(); i++)
				{
					groupHightlight[i].setPosition(groupHightlight[i].getPosition().x - incurmentX, groupHightlight[i].getPosition().y - incurmentY);
					if (i != groupNumber)//only change the entity position of the other highlighted entities, 
					{
						highLightedEntities[i]->SetPosition(sf::Vector2f(groupHightlight[i].getPosition().x + highLightedEntities[i]->GetImage()->GetWidth() / 2, groupHightlight[i].getPosition().y + highLightedEntities[i]->GetImage()->GetHeight() / 2));
					}
				}
			}
			if ((event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left)))
			{
				groupBoxState = 0;//when the mouse is released change the state to detect the next time the user clicks on a highlighted box
			}
			break;
		}
	}
}

void ObjectsOnScreen::RemoveEntity(Entity* removeEntity)///if a entity type is changed then is must be removed from every other TYPE list e.g. an NPC entity cannot be part of PLAYER entity list 
{														//Once the entity is added to the screen, its added to the AllEntites list. When the type is changed and Entity is created(NOT just change the types of existing one) 
	if (removeEntity != NULL)							//Due to different entity types requring different accompanying classes, the engine needs to create a new entity when changing type. This class delets the previous version of that entity
	{
		for (size_t i = 0; i < AllEntites.size(); i++)
		{
			if (AllEntites[i] == removeEntity)
			{
				AllEntites.erase(AllEntites.begin() + i);
			}
		}

		Entity_Type thisType = removeEntity->GetType();
		switch (thisType)
		{
		case PLAYER_TYPE:
			for (size_t i = 0; i < PlayerEntities.size(); i++)
			{
				if (PlayerEntities[i] == removeEntity)
				{
					PlayerEntities.erase(PlayerEntities.begin() + i);
				}
			}
		case NPC_TYPE:
			for (size_t i = 0; i < NPCEntites.size(); i++)
			{
				if (NPCEntites[i] == removeEntity)
				{
					NPCEntites.erase(NPCEntites.begin() + i);
				}
			}
			break;
		case OBJECT_TYPE:
			for (size_t i = 0; i < ObjectEntites.size(); i++)
			{
				if (ObjectEntites[i] == removeEntity)
				{
					ObjectEntites.erase(ObjectEntites.begin() + i);
				}
			}
			break;
		}
	}
}

void ObjectsOnScreen::LoadEntitiesFromFiles(string path)
{
	EntitySelected = nullptr;
	for (size_t i = 0; i < AllEntites.size(); i++)
	{
		delete AllEntites[i];
	}
	//do for other vectors

	fileLoader.GetEntities(&AllEntites, &PlayerEntities, &NPCEntites, &ObjectEntites, path);
}


void ObjectsOnScreen::CreateNewEntity(Sprite* sprite)
{
	Entity* newEntity = new Entity();
	newEntity->SetSprite(sprite);
	newEntity->GetImage()->SetOrigin(sf::Vector2f(sprite->GetSourceRect().x / 2, sprite->GetSourceRect().y / 2));
	newEntity->SetName("n/a" + to_string(AllEntites.size()));
	newEntity->SetType(UNDEFINED_TYPE);
	AllEntites.push_back(newEntity);
}

void ObjectsOnScreen::CreateNewPlayer(Sprite* playerSprite)
{
	Player* newPlayer = new Player();
	newPlayer->SetSprite(playerSprite);
	newPlayer->GetImage()->SetOrigin(sf::Vector2f(playerSprite->GetSourceRect().x / 2, playerSprite->GetSourceRect().y / 2));
	newPlayer->SetType(PLAYER_TYPE);
	newPlayer->SetName("Player" + to_string(PlayerEntities.size()));
	PlayerEntities.push_back(newPlayer);
	AllEntites.push_back(newPlayer);
}

void ObjectsOnScreen::CreateNewNPC(Sprite* NPCSprite)
{
	NPC* newNPC = new NPC();
	newNPC->SetSprite(NPCSprite);
	newNPC->GetImage()->SetOrigin(sf::Vector2f(NPCSprite->GetSourceRect().x / 2, NPCSprite->GetSourceRect().y / 2));
	newNPC->SetName("NPC" + to_string(NPCEntites.size()));
	newNPC->SetType(NPC_TYPE);
	NPCEntites.push_back(newNPC);
	AllEntites.push_back(newNPC);
}

void ObjectsOnScreen::CreateNewObject(Sprite* ObjectSprite)
{
	Object* newObject = new Object();
	newObject->SetSprite(ObjectSprite);
	newObject->GetImage()->SetOrigin(sf::Vector2f(ObjectSprite->GetSourceRect().x / 2, ObjectSprite->GetSourceRect().y / 2));
	newObject->SetName("Object" + to_string(ObjectEntites.size()));
	newObject->SetType(OBJECT_TYPE);
	ObjectEntites.push_back(newObject);
	AllEntites.push_back(newObject);
}

void ObjectsOnScreen::CreateEntityBasedOnType(Entity* entity)
{
	Entity_Type type = entity->GetType();
	Sprite* newImage = new Sprite();
	newImage->SetTexture(entity->GetImage()->GetTexutre());
	newImage->SetSourceRect(entity->GetImage()->GetSourceRect());
	newImage->SetOrigin(sf::Vector2f(entity->GetImage()->GetSourceRect().x / 2, entity->GetImage()->GetSourceRect().y / 2));
	newImage->SetPosition(mousePosition);
	newImage->SetScale(entity->GetImage()->GetScaleValue());
	newImage->SetFilePath(entity->GetImage()->GetFilePath());

	switch (type)
	{
	case UNDEFINED_TYPE:
		CreateNewEntity(newImage);
		break;
	case PLAYER_TYPE:
		CreateNewPlayer(newImage);
		break;
	case NPC_TYPE:
		CreateNewNPC(newImage);
		break;
	case OBJECT_TYPE:
		CreateNewObject(newImage);
		break;
	}
}

void ObjectsOnScreen::AddEntity(Sprite* newEntitySprite)//
{
	Entity* newEntity = new Entity();
	newEntity->SetSprite(newEntitySprite);
	newEntity->GetImage()->SetOrigin(sf::Vector2f(newEntity->GetImage()->GetSourceRect().x / 2, newEntity->GetImage()->GetSourceRect().y / 2));
	newEntity->SetName("n/a" + to_string(AllEntites.size()));
	AllEntites.push_back(newEntity);
}

vector<Entity*> ObjectsOnScreen::GetAllOfType(Entity_Type type)
{
	for (size_t i = 0; i < AllEntites.size(); i++)
	{ 
		//needs Dynamic cast checks
	}
	return vector<Entity*>();
}








