#include "Grid.h"

Grid::Grid(int Sizewidth, int Sizeheight, float screenWidth, float screenHight, int StartX, int StartY)
{
	_sizewidth = Sizewidth;
	_sizeheight = Sizeheight;
	_screenWidth = screenWidth;
	_screenHeight = screenHight;
	_startPositonX = StartX;
	_startPostiionY = StartY;

	_amountX = _screenWidth / _sizewidth;
	_amountY = _screenHeight / _sizeheight;

	idleColor = sf::Color(0, 0, 0, 0);//Colours for buttons 
	hoverColor = sf::Color(150, 150, 150, 200);
	PressedColor = sf::Color(20, 20, 20, 200);

	CreateLayout();
}

void Grid::CreateLayout()
{
	int Y = _startPostiionY;
	for (size_t i = 0; i < _sizeheight; i++)
	{
		vector<int> StateForLine;
		vector<sf::RectangleShape> newLine;
		int x = _startPositonX;
		for (size_t i = 0; i < _sizewidth; i++)
		{
			sf::RectangleShape newPosition;
			newPosition.setPosition(sf::Vector2f(x, Y));
			newPosition.setOutlineColor(sf::Color(158, 158, 158,50));
			newPosition.setOutlineThickness(0.5);
			newPosition.setFillColor(sf::Color(0, 0, 0, 0));

			newPosition.setSize(sf::Vector2f(_amountX, _amountY));
			newLine.push_back(newPosition);
			StateForLine.push_back(0);
			x += _amountX;
		}
		_GridPosition.push_back(newLine);
		_buttonStates.push_back(StateForLine);
		Y += _amountY;
	}
}

bool Grid::Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2)
{
	if (x1 < x2 + width2 &&
		x1 + width1 > x2&&
		y1 < y2 + hight2 &&
		y1 + hight1 > y2) {
		return true;
	}
	else
	{
		return false;
	}
}

void Grid::Draw(sf::RenderWindow* gameWindow)
{
	for (int i = 0; i < _GridPosition.size(); i++) {
		for (int j = 0; j < _GridPosition[i].size(); j++)
			gameWindow->draw(_GridPosition[i][j]);
	}
}

void Grid::Update(sf::Vector2f mousePos)
{
	for (int i = 0; i < _GridPosition.size(); i++) 
	{
		for (int j = 0; j < _GridPosition[i].size(); j++)
		{
			_buttonStates[i][j] = BTN_IDLE;

			if (_GridPosition[i][j].getGlobalBounds().contains(mousePos));
			{
				_buttonStates[i][j] = BTN_HOVER;
			}

			switch (_buttonStates[i][j])//change rectangle colour 
			{
			case BTN_IDLE:
				_GridPosition[i][j].setFillColor(idleColor);
				break;
			case BTN_HOVER:
				_GridPosition[i][j].setFillColor(hoverColor);
				break;
			case BTN_PRESSED:
				_GridPosition[i][j].setFillColor(PressedColor);
				break;
			}
		}
	}

	
} 