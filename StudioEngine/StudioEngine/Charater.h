#pragma once
//Creator : Jack Cross 

#include "Entity.h"
#include "Movement.h"
class Charater : public Entity
{
public:
	Charater();
	virtual void Update();

				/// <summary> Call this method to set the target to move towards </summary>
	void		Seek(sf::Vector2f Target ) {_movement->Seek(Target); }
	void		Separate(vector<Entity*> allEntities) { _movement->Seperation(allEntities); }
	void		SetSpeed(float speed) { _movement->SetSpeed(speed); }
	bool		DistanceToEntity(Entity* otherEntity, float distance);
	bool		DistanceCheckToPosition (sf::Vector2f pos, float distance);
	void		SetSepration(bool sep) { _separation = sep; _movement->SetSeperation(sep); }
	void		SetSeek(bool seek) { _movement->SetSeek(seek); }
	bool		GetSeparation() { return _separation; }
	void		SteeringUpdate(float time, Entity* Current);

				/// <summary> Rotate entity sprite to face position </summary>
	void		FacePosition(sf::Vector2f position);



private:
	bool		_separation;
	Movement*	_movement;
	
};

