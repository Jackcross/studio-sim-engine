#pragma once
//Creator : Liam Davy
#include "SFML/Audio.hpp"
class AudioSource
{
public: 
	std::string filePath;
	sf::SoundBuffer soundBuffer;
	sf::Sound sound;

	AudioSource(std::string path);
	~AudioSource();
	bool isPlaying();
	void Play();
	void Stop();
private: 

};

