#include "GunManager.h"
#include <math.h>
#include "Developer_Helper_Fucntion.h"


GunManager::GunManager(Entity* holder)
{
	audioManager = new AudioManager();
	audioManager->AddSoundToList("gun", "Resources/Sounds/pistol.wav", 1, false);//creating a new sound 
	player = holder;
	timeSinceLastBullet = 0;
	ammo = 100;
	speed = 0.2;
}

void GunManager::Draw(sf::RenderWindow* GameWindow)
{
	for (size_t i = 0; i < bullets.size(); i++)
	{
		GameWindow->draw(bullets[i].image->GetSprite());
	}
}

void GunManager::Update(double elaspedTime, sf::Vector2f trackPosition)
{
	time += elaspedTime;
	if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		if (ammo > 0)
		{
			CreateBullet(trackPosition);
			for (size_t i = 0; i < bullets.size(); i++)
			{
				bullets[i].distance = sqrt((bullets[i].angle.x * bullets[i].angle.x) + (bullets[i].angle.y * bullets[i].angle.y));
				bullets[i].direction.x = bullets[i].angle.x / bullets[i].distance;
				bullets[i].direction.y = bullets[i].angle.y / bullets[i].distance;
			}
		}
	}

	for (size_t i = 0; i < bullets.size(); i++)
	{
		bullets[i].position.x += bullets[i].direction.x * 340 * elaspedTime;
		bullets[i].position.y += bullets[i].direction.y * 340 * elaspedTime;
		bullets[i].image->SetPosition(bullets[i].position);
	}
}

void GunManager::CreateBullet(sf::Vector2f trackPosition)
{
	if (time > timeSinceLastBullet + speed)
	{
		audioManager->PlaySounds("gun");
		bullet newBullet;
		Sprite* bulletSprite = new Sprite("Resources/Textures/Bullet.png");//to add new images during runtime you need to create a new Sprite 
		bulletSprite->RotatePosition(trackPosition);
		bulletSprite->SetScale(1.5);
		newBullet.image = bulletSprite;
		newBullet.position = player->GetPosition();
		newBullet.distance = 0;
		newBullet.angle = trackPosition - player->GetPosition();
		newBullet.direction = sf::Vector2f(0, 0);
		bullets.push_back(newBullet);
		timeSinceLastBullet = time;
		ammo--;
	}
}

void GunManager::BulletCollision(vector<Zombie*>* zombies, vector<Sprite*>* ammoBox)
{
	for (size_t i = 0; i < bullets.size(); i++)
	{
		int width =1920;
		int height = 1080;
		float bulletDistance = distanceCheck(width / 2, height / 2, bullets[i].position.x, bullets[i].position.y);
		if (bulletDistance > 1050)
		{
			bullets.erase(bullets.begin() + i);
			break;
		}
		for (size_t j = 0; j < zombies->size(); j++)
		{
			if (Collision(bullets[i].position.x, bullets[i].position.y, 7, 7, (*zombies)[j]->zombie->GetPosition().x, (*zombies)[j]->zombie->GetPosition().y, (*zombies)[j]->zombie->GetImage()->GetSourceRect().x, (*zombies)[j]->zombie->GetImage()->GetSourceRect().y))
			{
				(*zombies)[j]->health -= 1;
				if ((*zombies)[j]->health <= 0)
				{
					zombies->erase(zombies->begin() + j);
				}
				int dropChance = rand() % (*zombies)[j]->DropChance;
				if (dropChance == 1)
				{
					Sprite* AmmoBoxSprite = new Sprite("Resources/Textures/ammoPickup.png");
					AmmoBoxSprite->SetPosition(bullets[i].position);
					ammoBox->push_back(AmmoBoxSprite);
				}
				bullets.erase(bullets.begin() + i);
				break;
			}
		}
	}
}





