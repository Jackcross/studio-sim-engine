#pragma once
#include "Structs.h"
#include "Entity.h"

class UndoController
{
public:
	UndoController();
	void Update(vector<Entity*> entities);
	void Undo();
	void UndoSetChange(WhatChanged change, void* data, Entity* currentEntity);
private:
	vector<Entity*> AllEntites;


	struct UndoStruct {//This structure will record one change to the entity on screen, this could movement,name,rotation etc. 
		WhatChanged Change;//Which type of change has been made

		//The passed Void* will contain one of the following 5 data types (e.g. position and sourcerect are both vector2f)
		sf::Vector2f vecData;
		float floatData;
		string stringData;
		Entity_Type typeData;
		std::vector<Entity*> groupMovedata;//For the group undo, we need Two lists to store a pointer to the exisiting entites, another for their positions
		std::vector<sf::Vector2f> groupMovePositionData;

		Entity* entityHold;//Store a copy of the entity so we can identiy which entity in AllEntites to apply the Undo

		UndoStruct(WhatChanged change, void* data, Entity* CurrentEntity)
		{
			if(CurrentEntity != nullptr)
			entityHold = CurrentEntity;

			Change = change;
			if (change == NAME)
			{
				stringData = *static_cast<string*>(data);
			}
			else if (change == POSITION || change == SOURCERECT)
			{
				vecData = *static_cast<sf::Vector2f*>(data);
			}
			else if (change == TYPE)
			{
				typeData = *static_cast<Entity_Type*>(data);
			}
			else if (change == SCALE || change == ROTATION)
			{
				floatData = *static_cast<float*>(data);
			}
			else if (change == GROUPMOVE)
			{
				groupMovedata = *static_cast<std::vector<Entity*>*>(data);
				for (size_t i = 0; i < groupMovedata.size(); i++)
				{
					groupMovePositionData.push_back(groupMovedata[i]->GetPosition());
				}
				
			}
		}
	};

	UndoStruct* newUndo;

	vector<UndoStruct*> undoList;

};



