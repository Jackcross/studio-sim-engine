#pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "Vector2D.h"
#include "Entity.h"
class Movement 
{
public:
	Movement();

	void			UpdatePosition(float time);
	
	void			MoveMovementSetPosition(sf::Vector2f pos) { _NewPosition.x = pos.x; _NewPosition.y = pos.y; }

	sf::Vector2f	UpdateSteering(float time, Entity* Current);

	sf::Vector2f	Seek(sf::Vector2f TargetPos);
	void			SetSpeed(float speed) { _maxSpeed = speed; }
	float			GetSpeed() { return _maxSpeed; }
	void			Seperation(vector<Entity*> drawList);
	void			Seperation(vector<sf::Vector2f> objectList);
	void			SetSeperation(bool sep) { _separationBehav = sep; }
	void			SetSeek(bool seek) { _seekBahav = seek; }
	bool			DistanceCheck(Entity* otherEntity, Entity* Current,float distance);
	bool			DistanceCheckWithPos(Entity* Current, sf::Vector2f position, float distance);
	
private:
	float								dist(sf::Vector2f a, sf::Vector2f b);
	void								TagClosest(vector<sf::Vector2f> OtherEntities);
	void								Add(Vector2D add);
	//sf::Vec
	Vector2D							_NewPosition;

	Entity*								_currentEntity;

	vector<Entity*>						_OtherEntites;

	Vector2D							_velocity;
	Vector2D							_acceleration;

	float								_maxSpeed;
	double								_CurrentSpeed;
	float								_maxForce;

	Vector2D*							_TargetPos;
	Vector2D*							_TargetVel;

	bool								_separationBehav;
	bool								_seekBahav;

	Vector2D							_seekForce;
	Vector2D							_separationForce;
};

