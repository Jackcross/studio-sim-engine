#include "Entity.h" 
#include "Structs.h" 
class CurrentEntites
{
public: 
CurrentEntites(); 
};
CurrentEntites::CurrentEntites()
 {
	 vector<Entity*> _entities; 
	 Entity* newEntity = new Entity(); 

	 //Entity Number : 0
	 Sprite* Sprite0 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("n/a2");
	 newEntity->SetSprite(Sprite0);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(1129,177));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 1
	 Sprite* Sprite1 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("n/a14");
	 newEntity->SetSprite(Sprite1);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(207,227));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 2
	 Sprite* Sprite2 = new Sprite("Resources/Textures/Player.png");
	 newEntity->SetName("Player0");
	 newEntity->SetSprite(Sprite2);
	 newEntity->GetImage()->SetFilePath("Player.png");
	 newEntity->SetPosition(sf::Vector2f(894,780));
	 newEntity->SetType((Entity_Type)1);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(75,100));

	 //Entity Number : 3
	 Sprite* Sprite3 = new Sprite("Resources/Textures/grassTile.png");
	 newEntity->SetName("n/a43");
	 newEntity->SetSprite(Sprite3);
	 newEntity->GetImage()->SetFilePath("grassTile.png");
	 newEntity->SetPosition(sf::Vector2f(965,49));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(1);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(64,64));

	 //Entity Number : 4
	 Sprite* Sprite4 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC0");
	 newEntity->SetSprite(Sprite4);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(1118,86));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 5
	 Sprite* Sprite5 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC1");
	 newEntity->SetSprite(Sprite5);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(1150,240));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 6
	 Sprite* Sprite6 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC2");
	 newEntity->SetSprite(Sprite6);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(1088,345));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 7
	 Sprite* Sprite7 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC3");
	 newEntity->SetSprite(Sprite7);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(984,438));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 8
	 Sprite* Sprite8 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC4");
	 newEntity->SetSprite(Sprite8);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(897,348));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 9
	 Sprite* Sprite9 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC5");
	 newEntity->SetSprite(Sprite9);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(919,302));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 10
	 Sprite* Sprite10 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC6");
	 newEntity->SetSprite(Sprite10);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(1056,204));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 11
	 Sprite* Sprite11 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC7");
	 newEntity->SetSprite(Sprite11);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(965,229));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 12
	 Sprite* Sprite12 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC8");
	 newEntity->SetSprite(Sprite12);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(1035,92));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 13
	 Sprite* Sprite13 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC9");
	 newEntity->SetSprite(Sprite13);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(958,123));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 14
	 Sprite* Sprite14 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC10");
	 newEntity->SetSprite(Sprite14);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(1031,290));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 15
	 Sprite* Sprite15 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC11");
	 newEntity->SetSprite(Sprite15);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(995,169));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 16
	 Sprite* Sprite16 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC12");
	 newEntity->SetSprite(Sprite16);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(895,179));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 17
	 Sprite* Sprite17 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC13");
	 newEntity->SetSprite(Sprite17);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(881,257));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 18
	 Sprite* Sprite18 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC14");
	 newEntity->SetSprite(Sprite18);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(805,175));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 19
	 Sprite* Sprite19 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC15");
	 newEntity->SetSprite(Sprite19);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(799,243));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 20
	 Sprite* Sprite20 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC16");
	 newEntity->SetSprite(Sprite20);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(859,108));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 21
	 Sprite* Sprite21 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC17");
	 newEntity->SetSprite(Sprite21);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(760,208));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 22
	 Sprite* Sprite22 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC18");
	 newEntity->SetSprite(Sprite22);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(727,246));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 23
	 Sprite* Sprite23 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC19");
	 newEntity->SetSprite(Sprite23);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(717,132));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 24
	 Sprite* Sprite24 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC20");
	 newEntity->SetSprite(Sprite24);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(780,83));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 25
	 Sprite* Sprite25 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC21");
	 newEntity->SetSprite(Sprite25);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(583,138));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 26
	 Sprite* Sprite26 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC22");
	 newEntity->SetSprite(Sprite26);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(566,234));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 27
	 Sprite* Sprite27 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC23");
	 newEntity->SetSprite(Sprite27);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(522,91));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 28
	 Sprite* Sprite28 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC24");
	 newEntity->SetSprite(Sprite28);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(345,198));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 29
	 Sprite* Sprite29 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC25");
	 newEntity->SetSprite(Sprite29);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(380,151));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 30
	 Sprite* Sprite30 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC26");
	 newEntity->SetSprite(Sprite30);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(277,142));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 31
	 Sprite* Sprite31 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC27");
	 newEntity->SetSprite(Sprite31);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(156,120));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 32
	 Sprite* Sprite32 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC28");
	 newEntity->SetSprite(Sprite32);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(-129,117));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(8.1);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 33
	 Sprite* Sprite33 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC29");
	 newEntity->SetSprite(Sprite33);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(55,215));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(6.05);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 34
	 Sprite* Sprite34 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC30");
	 newEntity->SetSprite(Sprite34);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(199,259));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(4.15);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 35
	 Sprite* Sprite35 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC31");
	 newEntity->SetSprite(Sprite35);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(319,307));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(2.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 36
	 Sprite* Sprite36 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC32");
	 newEntity->SetSprite(Sprite36);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(400,344));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(1.85);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 37
	 Sprite* Sprite37 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC33");
	 newEntity->SetSprite(Sprite37);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(450,358));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(1.2);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 38
	 Sprite* Sprite38 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC34");
	 newEntity->SetSprite(Sprite38);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(484,377));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.7);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 39
	 Sprite* Sprite39 = new Sprite("Resources/Textures/Zombie2.png");
	 newEntity->SetName("NPC35");
	 newEntity->SetSprite(Sprite39);
	 newEntity->GetImage()->SetFilePath("Zombie2.png");
	 newEntity->SetPosition(sf::Vector2f(505,384));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.45);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 40
	 Sprite* Sprite40 = new Sprite("Resources/Textures/Zombie.png");
	 newEntity->SetName("NPC36");
	 newEntity->SetSprite(Sprite40);
	 newEntity->GetImage()->SetFilePath("Zombie.png");
	 newEntity->SetPosition(sf::Vector2f(520,390));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.25);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(51,60));

	 //Entity Number : 41
	 Sprite* Sprite41 = new Sprite("Resources/Textures/Player.png");
	 newEntity->SetName("NPC37");
	 newEntity->SetSprite(Sprite41);
	 newEntity->GetImage()->SetFilePath("Player.png");
	 newEntity->SetPosition(sf::Vector2f(813,709));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(75,100));

	 //Entity Number : 42
	 Sprite* Sprite42 = new Sprite("Resources/Textures/Player.png");
	 newEntity->SetName("NPC38");
	 newEntity->SetSprite(Sprite42);
	 newEntity->GetImage()->SetFilePath("Player.png");
	 newEntity->SetPosition(sf::Vector2f(1024,724));
	 newEntity->SetType((Entity_Type)3);
	 newEntity->GetImage()->SetScale(0.75);
	 newEntity->GetImage()->SetSourceRect(sf::Vector2f(75,100));

}
