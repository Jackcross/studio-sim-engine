#pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "Button.h"
#include "DropList.h"
#include "Sprite.h"
#include "ObjectsOnScreen.h"
#include "TextBox.h"
#include "ImagesMenu.h"
#include "SelectedMenu.h"
#include "MapMenu.h"
#include "ObjectListMenu.h"


class Menus
{
public:
	Menus(ObjectsOnScreen* ScreenObjects, TileMap* map);
	void update(sf::Vector2f mousePos, double deltaTime);
	void Draw(sf::RenderWindow* gameWindow);
	void SetEevnt(sf::Event* event) { this->event = event; }
	MenuState GetEngineState() { return state; }
	void ResizeToWindow(sf::RenderWindow* gameWindow);

private:

	void ButtonsSetup();
	void MenuLayout();
	void MainBtnListUpdate(sf::Vector2f mousePos);
	void MainBtnListDraw(sf::RenderWindow* gameWindow);

	void SetUpEntityList();
	void UpdateEntityList(sf::Vector2f mousePos);

	void SlowUpdate(double deltaTime);
	double timePassed = 0;

	LoadFromFile fileLoader;

	sf::Font MyFont;

	ImagesMenu* imageMenu;
	SelectedMenu* selectedMenu;
	MapMenu* mapMenu;
	ObjectListMenu* listMenu;

	vector<sf::RectangleShape*> LayoutShapes;

	Button* CloseMenubtn;
	Button* OpenMenubtn;

	sf::RectangleShape* sideMenu;
	sf::RectangleShape* TopMenu;

	sf::Vector2f mousePosition;

	ObjectsOnScreen* ScreenObjects;

	sf::Event* event;

	vector<Button*> MainMenuBTN;
	Button* Imagesbtn;
	Button* Selectedbtn;
	Button* Mapbtn;
	Button* Playbtn;
	Button* Stopbtn;
	Button* Savebtn;
	Button* Loadbtn;
	DropList* EntityLoadList;
	TextBox* SaveTextbox;
	MenuState state;
	MenuState OpenCloseState;
	sf::Text  EntitesOnlytext;
	sf::RectangleShape* SideMenuBoarder;

	int ScreenWidth;
	int ScreenHeight;


};

