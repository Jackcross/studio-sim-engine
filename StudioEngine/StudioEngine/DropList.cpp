#include "DropList.h"
#include <Windows.h>
//Creator : Jack Cross 

DropList::DropList(int amount, sf::Vector2f position, sf::Vector2f size)
{
	if (!_MyFont.loadFromFile("Resources/Fonts/Roboto-MediumItalic.ttf"))//Load font
	{
		int test = 0;
		// Error...
	}

	_listState = CLOSED;//start with the list closed 
	this->_amount = amount;
	this->_position = position;
	this->_size = size;
	if (amount <= 0)
	{
		this->_amount = 1;//always make sure atleast one rectangle is created and displayed to keep it looking like a menu
	}

	_idleColor = sf::Color(255, 255, 255, 255);//Colours for buttons 
	_hoverColor = sf::Color(150, 150, 150, 200);
	_PressedColor = sf::Color(20, 20, 20, 200);
	_changeBool = false;

	CreateList();


}

void DropList::CreateList()
{
	int Ypositon = _position.y;
	for (size_t i = 0; i < _amount; i++)//Create each option
	{
		sf::Text newtext;
		newtext.setString("n/a");
		newtext.setPosition(sf::Vector2f(_position.x + 5, Ypositon ));
		newtext.setCharacterSize(14);
		newtext.setFont(_MyFont);
		newtext.setFillColor(sf::Color(0, 0, 0, 255));
		sf::RectangleShape* newOption = new sf::RectangleShape();//using rectangles for the visual options with a white background 
		newOption->setSize(_size);
		newOption->setPosition(sf::Vector2f(_position.x, Ypositon));
		newOption->setFillColor(sf::Color(255, 255, 255, 255));
		Ypositon += _size.y + 1;//move the Y position for the next option created 
		_ListRec.push_back(newOption);
		_buttonState.push_back(0);
		_ListText.push_back(newtext);
	}

	//setup arrow sprite
	_DropDownArrowSprite = new Sprite("Resources/EngineTextures/DownArrow.png", sf::Vector2f(34, 35));
	_DropDownArrowSprite->SetPosition(sf::Vector2f(_ListRec[0]->getPosition().x + _ListRec[0]->getGlobalBounds().width + 10, _ListRec[0]->getPosition().y + 7));
	_DropDownArrowSprite->SetScale(0.5);

}

void DropList::Update(const sf::Vector2f mousePos)
{
	_changeBool = false;//used to check if there has been a change to the selected option (selected option always at the top of list)
	switch (_listState)
	{
	case CLOSED:
		_ListRec[0]->setFillColor(_idleColor);
		if (_ListRec[0]->getGlobalBounds().contains(mousePos) || _DropDownArrowSprite->GetSprite().getGlobalBounds().contains(mousePos))//Check for first button or arrow mouse click
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))//if the menu is clicked - open it 
			{
				_listState = OPEN;
			}
		}

		for (size_t i = 1; i < _ListRec.size(); i++)
		{
			_ListRec[i]->setFillColor(_idleColor);
		}
		break;


	case OPEN:
		Sleep(100);//Use sleep to stop the next if statment registering the first click to open the menu
		if (_ListRec[0]->getGlobalBounds().contains(mousePos) || _DropDownArrowSprite->GetSprite().getGlobalBounds().contains(mousePos))//if the mouse is clicked anywhere outside of the drop list - close the list 
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				_listState = CLOSED;
			}
		}
		else
		{
			if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
			{
				_listState = CLOSED;
			}
		}
		for (size_t i = 0; i < _ListRec.size(); i++)
		{
			_buttonState[i] = BTN_IDLE;

			if (_ListRec[i]->getGlobalBounds().contains(mousePos))//change colour state depending on mouse position 
			{
				_buttonState[i] = BTN_HOVER;

				if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
				{
				
					_buttonState[i] = BTN_PRESSED;

					//move the selected option to the top of the list(Position of click option switches with top option) //this keeps the selected option visable even when the drop list is closed
					string Selected; 
					string Move;		
					Selected = _ListText[i].getString();
					Move = _ListText[0].getString();

					_ListText[i].setString(Move);
					_ListText[0].setString(Selected);
					_changeBool = true;
				}
			}

			switch (_buttonState[i])//change rectangle colour 
			{
			case BTN_IDLE:
				_ListRec[i]->setFillColor(_idleColor);
				break;
			case BTN_HOVER:
				_ListRec[i]->setFillColor(_hoverColor);
				break;
			case BTN_PRESSED:
				_ListRec[i]->setFillColor(_PressedColor);
				break;
			}
		}
		break;
	}


}

void DropList::Draw(sf::RenderWindow* gameWindow)
{
	switch (_listState)
	{
	case CLOSED:

		gameWindow->draw(*_ListRec[0]);// always draw the top option, text and rectangle when closed 
		gameWindow->draw(_ListText[0]);
		break;


	case OPEN:

		for (size_t i = 0; i < _ListRec.size(); i++)
		{
			gameWindow->draw(*_ListRec[i]);
		}

		for (size_t i = 0; i < _ListText.size(); i++)
		{
			gameWindow->draw(_ListText[i]);
		}

		break;
	}
	gameWindow->draw(_DropDownArrowSprite->GetSprite());


}

void DropList::SetPosition(sf::Vector2f position)
{
	int Ypositon = position.y;
	for (size_t i = 0; i < _amount; i++)
	{

		_ListText[i].setPosition(sf::Vector2f(position.x + 5, Ypositon));


		_ListRec[i]->setPosition(sf::Vector2f(position.x, Ypositon));

		Ypositon += _size.y + 1;//move the Y position for the next option  
	}
	_DropDownArrowSprite->SetPosition(sf::Vector2f(_ListRec[0]->getPosition().x + _ListRec[0]->getGlobalBounds().width + 10, _ListRec[0]->getPosition().y + 7));
}

void DropList::SetTextAt(int index, string text)
{
	_ListText[index].setString(text);
}

void DropList::SetAllText(vector<string> text)
{
	for (size_t i = 0; i < _ListText.size(); i++)
	{
		_ListText[i].setString(text[i]);
	}
}

void DropList::AddTextOption(string text)/////NOT WORKING
{
	if (_ListText[0].getString() == "n/a")
	{
		_ListText[0].setString(text);
	}
	else
	{
		sf::Text newtext;
		newtext.setString(text);
		newtext.setPosition(sf::Vector2f(_position.x + 3, (_ListRec[_ListRec.size() - 1]->getPosition().y + _size.y) + 2));
		newtext.setCharacterSize(14);
		newtext.setFont(_MyFont);
		newtext.setFillColor(sf::Color(0, 0, 0, 255));
		sf::RectangleShape* newOption = new sf::RectangleShape();
		newOption->setSize(_size);
		newOption->setPosition(sf::Vector2f(_position.x, _ListRec[_ListRec.size() - 1]->getPosition().y + _size.y));
		newOption->setFillColor(sf::Color(255, 255, 255, 255));
		_ListRec.push_back(newOption);
		_buttonState.push_back(0);
		_ListText.push_back(newtext);
	}

}

void DropList::SetSelectedOption(string text)//check if text exists then change it to the top selected option 
{
	for (size_t i = 0; i < _ListText.size(); i++)
	{
		if (_ListText[i].getString() == text)
		{
			string Selected;
			string Move;
			Selected = text;
			Move = _ListText[0].getString();

			_ListText[i].setString(Move);
			_ListText[0].setString(Selected);
		}
	}
}

void DropList::AddIfNoneExistent(string name)
{
	bool exists = false;
	for (size_t i = 0; i < _ListText.size(); i++)
	{
		if (_ListText[i].getString() == name)
		{
			exists = true;
		}
	}
	if (exists == false)
	{
		AddTextOption(name);
	}
}
