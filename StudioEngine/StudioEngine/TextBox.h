#pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "Button.h"
class TextBox
{
public:
	TextBox(float posX, float posY, float width, float height, sf::Font* myFont, string text, bool Button, sf::Event* event, string ButtonText);

	void			Draw(sf::RenderWindow* gameWindow);
	void			Update(sf::Vector2f mousePos, sf::Event* event);
	bool			CheckSelected();
	sf::Text		GetInputString() { return _text; }
	float			GetInputFloat() { return _storedValue; }
	void			SetFloat(float x);
	void			SetString(string text) { _text.setString(text); _inputHold = text; }
	float			GetWidthWithButtons() { return _box.getGlobalBounds().width + _buttonLeft->getRec()->getGlobalBounds().width + _buttonRight->getRec()->getGlobalBounds().width + 8; }
	void			SetAddAmount(float amount) { _AddAmount = amount; }
	void			ChangeBTNType(Button_Type type);
	Button_Type		GetBTNType() { return _BTNType; }//not in use/not working
	sf::RectangleShape GetRect() { return _box; }
	void			SetTextAmount(int amount) { _TextAmount = amount; }
	void			SetPosition(sf::Vector2f pos);
	Button_side		GetArrowPress();
	bool			isTextBoxSelected() { return _isSelected; };


private:

	string FloatToString(float num);


	sf::Vector2f _position;
	sf::Vector2f _size;
	sf::Text 	_text;
	float       _storedValue;
	bool		_buttonBool;
	string		_buttonString;
	sf::Font*	_myFont;
	bool		_isSelected;
	sf::Event*	_Event;
	float		_AddAmount;
	string		_inputHold;
	int			_TextAmount;

	sf::RectangleShape _box;

	Button_Type _BTNType = ARROWS_BTN;

	Button* _buttonLeft;
	Button* _buttonRight;
	Button* _buttonSet;
};

