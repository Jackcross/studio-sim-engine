#include "Sprite.h"
#include <iostream>
//Creator : Kamil Chmielewski

Sprite::Sprite(std::string filePath, sf::Vector2f spriteRect) : _sourceRect(spriteRect)
{
	if (!_texture.loadFromFile(filePath))
	{
		std::cout << "Failed to load image: " << filePath.c_str() << std::endl;
		return;
	}

	const size_t last_slash_idx = filePath.rfind('//', filePath.length());
	if (std::string::npos != last_slash_idx)
	{
		_filePathString = filePath.substr(last_slash_idx +1, filePath.length());
	}

	_imageSize.x = _texture.getSize().x;
	_imageSize.y = _texture.getSize().y;
	
	if (spriteRect.y != _imageSize.y)
	{
		_sourceRect = _imageSize;
	}

	_maxAnimation.x = _imageSize.x / _sourceRect.x;
	_maxAnimation.y = _imageSize.y / _sourceRect.y;

	_sprite.setTexture(_texture);
	_sprite.setTextureRect(sf::IntRect(0.0f, 0.0f, _sourceRect.x, _sourceRect.y));

	_switchTime = 0.3f;
	_totalTime = 0.0f;
	_currentImageWidth = _sprite.getGlobalBounds().width;
	_currentImageHeight = _sprite.getGlobalBounds().height;
	_scaleAmount = 1.0;



}

Sprite::Sprite(sf::Vector2f spriteRect) : _sourceRect(spriteRect) 
{
	
}


Sprite::~Sprite()
{

}


void Sprite::Update(float DeltaTime, int row)
{
	/*Currently the animation transform will be done here but later be moved into player*/
	_AnimationImageHeight = row;
	_totalTime += DeltaTime;

	if (_totalTime >= _switchTime)
	{
		_totalTime -= _switchTime;

		_currentImageWidth++;

		if (_AnimationImageWidth >= _maxAnimation.x)
			_AnimationImageWidth = 0;

	}

	_SpriteSheetposition.x = _AnimationImageWidth * _sourceRect.x;
	_SpriteSheetposition.y = 0;

	//_sprite.setPosition(400.0f, 400.0f);
	_sprite.setTextureRect(sf::IntRect(_SpriteSheetposition.x, _SpriteSheetposition.y, _sourceRect.x, _sourceRect.y));

}

void Sprite::SetTexture(sf::Texture tex)
{
	
	_texture.swap(tex);
	_sprite.setTexture(_texture);
	_currentImageWidth = _sprite.getGlobalBounds().width;
	_currentImageHeight = _sprite.getGlobalBounds().height;
}

void Sprite::SetSourceRect(float x, float y)
{
	_sourceRect.x = x;
	_sourceRect.y = y; 
	_sprite.setTextureRect(sf::IntRect((int)_SpriteSheetposition.x, (int)_SpriteSheetposition.y, (int)_sourceRect.x, (int)_sourceRect.y));
	_currentImageWidth = _sprite.getGlobalBounds().width;
	_currentImageHeight = _sprite.getGlobalBounds().height;
}


void Sprite::RotatePosition(sf::Vector2f position)
{
	const float PI = 3.14159265;

	float dot = _sprite.getPosition().x * position.x + _sprite.getPosition().y * position.y;
	float det = _sprite.getPosition().x * position.y - _sprite.getPosition().y * position.x;

	float dx = position.x - _sprite.getPosition().x;
	float dy = position.y - _sprite.getPosition().y;

	float angle = atan2f(dy, dx);
	angle = angle * (180 / PI);

	if (angle < 0)
		angle = 360 - (-angle);

	SetRotation(angle);
}