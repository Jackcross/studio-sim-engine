#pragma once
//Creator : Jack Cross 

#define NOMINMAX
#include <iostream>
#include <Windows.h>
#include <SFML/Graphics.hpp>
#include "SFML/System/Clock.hpp"
#include "Sprite.h"
#include "Menus.h"
#include "Entity.h"
#include "Renderer.h"
#include "ObjectsOnScreen.h"
#include "Grid.h"
#include "TileMap.h"
#include "GameMain.h"

class EngineManage
{
public:
	EngineManage(int argc, char* argv[]);
	~EngineManage();

	void LoadContent();
	void Update(double elapsedTime);
	void Draw();
	void OnScreenEntites();


private:

	//Sprite* _sprite;
	Menus* menu;
	sf::Vector2f mousePosition;
	ObjectsOnScreen* ScreenObjects;
	sf::Event* event;
	sf::RenderWindow* window;
	Grid* newGrid;
	TileMap* tileMap;
	GameMain* newGame;
};

