#pragma once
//Creator : Kamil Chmielewski
#include <SFML/Graphics/Texture.hpp>
#include <SFML/Graphics/Sprite.hpp>
#include <string>

class Sprite
{
private:
	sf::Texture  _texture; //can be used same as GLBindTexture, Future Optimization is too only have one
	sf::Vector2f _SpriteSheetposition; //cooridnates of the position in the spritesheet
	sf::Vector2f _sourceRect;
	sf::Vector2f _imageSize;
	sf::Vector2f _maxAnimation;
	sf::Sprite _sprite;
	sf::IntRect _animationRect;
	float _scaleAmount;
	int _AnimationImageWidth;
	int _AnimationImageHeight;
	float _currentImageWidth;
	float _currentImageHeight;
	float _totalTime;
	float _switchTime;
	std::string _filePathString;
	sf::Vector2f _SpritePosition;

public:
	Sprite(std::string filePath, sf::Vector2f spriteRect = sf::Vector2f(32.0f, 32.0f));
	Sprite(sf::Vector2f spriteRect = sf::Vector2f(32.0f, 32.0f));

	~Sprite();


	sf::Vector2f	GetPostion() { return _sprite.getPosition(); }
	void			SetPosition(sf::Vector2f pos) { _sprite.setPosition(pos); _SpritePosition = pos; }
	void			SetPosition(float x, float y) { _sprite.setPosition(x, y); _SpritePosition = sf::Vector2f(x, y); }
	void			SetPositionX(float x) { _sprite.setPosition(x, _sprite.getPosition().y); }
	void			SetPositionY(float y) { _sprite.setPosition(_sprite.getPosition().x, y); }
	float			GetRotation() { return _sprite.getRotation(); }
	void			SetRotation(float rot) { _sprite.setRotation(rot); }
	void			RotatePosition(sf::Vector2f position);


	float			GetHeight() { return _currentImageHeight; }
	float			GetWidth() { return _currentImageWidth; }


	void			SetScale(sf::Vector2f scale) { _sprite.setScale(scale); _scaleAmount = scale.x; SetOrigin(_sourceRect.x / 2, _sourceRect.y / 2); _currentImageWidth = _sprite.getGlobalBounds().width; _currentImageHeight = _sprite.getGlobalBounds().height;}
	void			SetScale(float scale) { _sprite.setScale(sf::Vector2f(scale, scale)); _scaleAmount = scale; SetOrigin(_sourceRect.x / 2, _sourceRect.y / 2);	_currentImageWidth = _sprite.getGlobalBounds().width; _currentImageHeight = _sprite.getGlobalBounds().height; }

	float			GetScaleValue() { return _scaleAmount; }

	sf::Sprite		GetSprite() { return _sprite; }
	sf::Texture		GetTexutre() { return _texture; }
	void			SetTexture(sf::Texture tex);

	void			SetSourceRect(sf::Vector2f pos) { _sourceRect = pos;  SetSourceRect(pos.x, pos.y); }
	void			SetSourceRect(float x, float y);
	void			SetSourceRectX(float x){  _sourceRect.x = x; SetSourceRect(x, _sourceRect.y); }
	void			SetSourceRectY(float y) { _sourceRect.y = y; SetSourceRect(_sourceRect.x, y); }
	sf::Vector2f	GetSourceRect() { return _sourceRect; }

	sf::IntRect		GetAnimationRect() { return _animationRect; }

	void			SetFilePath(std::string path) { _filePathString = path; }
	std::string 	GetFilePath() { return _filePathString; }

	void			SetOrigin(float x, float y) { _sprite.setOrigin(sf::Vector2f(x, y)); }
	void			SetOrigin(sf::Vector2f vec) { _sprite.setOrigin(vec); }

	void			SetTextureSmoothness(bool state) { _texture.setSmooth(state); }

	void			Update(float DeltaTime, int row = 1);


};