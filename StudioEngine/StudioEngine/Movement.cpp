#include "Movement.h"
//Creator : Jack Cross 
#include <math.h>
#define MOVETIMES 180;

static Vector2D subt(Vector2D& a, Vector2D& b)
{
	Vector2D outCome;
	outCome.x = a.x - b.x;
	outCome.y = a.y - b.y;

	return outCome;
}
Movement::Movement()
{
	_maxSpeed = 2.2;
	_maxForce = 1.5;
	_velocity = Vector2D(0, 0);
	_acceleration = Vector2D(0, 0);
}


sf::Vector2f Movement::UpdateSteering(float time, Entity* Current)
{
	int width = GetSystemMetrics(SM_CXSCREEN);
	int height = GetSystemMetrics(SM_CYSCREEN);

	_currentEntity = Current;
	MoveMovementSetPosition(Current->GetPosition());

	if (_seekBahav == true)
	{
		_seekForce.mult(0.08);
		Add(_seekForce);
	}

	if (_NewPosition.x > 0 && _NewPosition.x < width && _NewPosition.y > 0 && _NewPosition.y < height)
	{
		if (_separationBehav == true)
		{
			_separationForce.mult(0.27);
			Add(_separationForce);
		}
	}

	_velocity.add(_acceleration);
	_velocity.Limit(_maxSpeed);

	_NewPosition.x += _velocity.x * time * MOVETIMES;
	_NewPosition.y += _velocity.y * time * MOVETIMES;

	_acceleration.mult(0);

	return sf::Vector2f(_NewPosition.x, _NewPosition.y);
}

sf::Vector2f Movement::Seek(sf::Vector2f TargetPos)
{
	_seekForce = Vector2D(0, 0);
	Vector2D target(TargetPos.x, TargetPos.y);
	Vector2D desired = target - Vector2D(_currentEntity->GetPosition().x, _currentEntity->GetPosition().y);
	desired.Normalize();
	desired.mult(_maxSpeed);
	Vector2D steer = desired - _velocity;
	steer.Limit(_maxForce);
	_seekForce = steer;
	return sf::Vector2f(_NewPosition.x, _NewPosition.y);

}



void Movement::Seperation(vector<Entity*> drawList)
{
	//SetPosition(Current->GetPosition());
	_separationForce = Vector2D(0, 0);
	float desiredseparation = 10;
	Vector2D sum;
	Vector2D myPosition(_currentEntity->GetPosition().x, _currentEntity->GetPosition().y);
	int count = 0;
	for (unsigned int i = 0; i < drawList.size(); i++) {
		Vector2D pPosition((drawList[i]->GetPosition().x), (drawList[i]->GetPosition().y));
		float d = dist(_currentEntity->GetPosition(), sf::Vector2f(pPosition.x, pPosition.y));

		if ((d > 0) && (d < desiredseparation)) {
			Vector2D diff = subt(myPosition, pPosition);
			diff.Normalize();
			diff.div(d);
			sum.add(diff);
			count++;
		}
	}
	if (count > 0) {
		sum.div(count);
		sum.Normalize();
		sum.mult(_maxSpeed);
		Vector2D steer = subt(sum, _velocity);
		steer.Limit(_maxForce);
		_separationForce = steer;
		//Add(steer);
		//UpdatePosition(time);
		//Current->SetPosition(sf::Vector2f(_position.x, _position.y));
	}
	else
	{
		return Add(Vector2D(0, 0));
		_separationForce = Vector2D(0, 0);
	}
}



bool Movement::DistanceCheck(Entity* otherEntity, Entity* Current, float distance)
{
	float d = dist(Current->GetPosition(), otherEntity->GetPosition());
	if (d < distance)
	{
		return true;
	}
	else
	{
		return false;
	}
}

bool Movement::DistanceCheckWithPos(Entity* Current, sf::Vector2f position, float distance)
{
	float d = dist(Current->GetPosition(), position);
	if (d < distance)
	{
		return true;
	}
	else
	{
		return false;
	}
}

void Movement::TagClosest(vector<sf::Vector2f> OtherEntities)
{
}

void Movement::Add(Vector2D add)
{
	_acceleration.add(add);
}

float Movement::dist(sf::Vector2f a, sf::Vector2f b)
{
	double ySeparation = a.y - b.y;
	double xSeparation = a.x - b.x;

	return sqrt(ySeparation * ySeparation + xSeparation * xSeparation);
}
