 #pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "TextBox.h"
#include "DropList.h"
#include "ObjectsOnScreen.h"
#include "Entity.h"

class SelectedMenu
{
public:
	SelectedMenu(sf::Font font, ObjectsOnScreen* objectOnscreen);// You need an instance of ObjectOnScreen to access all values of each entity to be displayed in textboxes
	void			Update(sf::Vector2f mousePos, sf::Event* event);
	void			Draw(sf::RenderWindow* gameWindow);
	void			SetSelectedToNull() { selectedEntity = nullptr; }
	void			SetupTextBox();
	void			ResizeToWindow(sf::RenderWindow* gameWindow);

private:
	void			SetupReferenceShaps();
	void			SpriteSelected();
	void			UpdateMenuOptionsWithNewObject();
	void			ListSetup();
	void			CheckSelectedEnitySize();
	string			FloatToString(float num);
	 
	Sprite*				displaySprite;

	vector<TextBox*>	SelectedTextboxs;//all the different text boxes for this menu 
	TextBox*			SetPositionBOXX;
	TextBox*			SetPositionBOXY;
	TextBox*			SetSourceRectBOXX;
	TextBox*			SetSourceRectBOXY;
	TextBox*			SetNameBOX;
	TextBox*			SetScaleBOX;
	TextBox*			SetRotationBOX;

	vector<sf::Text*>	Textboxs;//all the different display text for name for each text box 
	sf::Text*			SetPositionText;
	sf::Text*			SetSourceRectText;
	sf::Text*			SetNameText;
	sf::Text*			SetScaleText;
	sf::Text*			SetRotationText;

	vector<DropList*>	DropLists;
	DropList*			TypeSelectList;

	ObjectsOnScreen*	objectOnscreen;

	Entity*				selectedEntity;

	sf::Vector2f		mousePosition;
	sf::Font			MyFont;
	sf::Event* event;

	sf::RectangleShape* sideMenu;

	bool				CheckSelected; //used to check if Name Textbox is selected for undo function

};

