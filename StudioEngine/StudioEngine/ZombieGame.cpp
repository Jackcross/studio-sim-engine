#include "ZombieGame.h"
#include "Developer_Helper_Fucntion.h"
ZombieGame::ZombieGame(vector<Entity*> entities, TileMap* map)
{
	srand((unsigned)time(0));

	this->map = map;
	this->entities = entities;//I'm passing map and entities to my own instances
	CurrentMap = 0;
	castEntites();
	CreateNewGame();
	SetupText();

	vector<Tile*> grid = map->GetGrid();

	for (size_t i = 0; i < grid.size(); i++)
	{
		if (grid[i]->GetTileSprite()->GetFilePath() == "lavaTile.png")
		{
			LAVATiles.push_back(grid[i]);
			Object* newTileObj = new Object();
			newTileObj->SetSprite(grid[i]->GetTileSprite());
			newTileObj->SetPosition(grid[i]->GetTileSprite()->GetPostion());
			wave1->GetZombieNPCList()->push_back(newTileObj);
		}
	}
	int tesyt = 0;
}

void ZombieGame::Draw(sf::RenderWindow* gameWindow)
{
	fighter->Draw(gameWindow);
	gun->Draw(gameWindow);
	wave1->Draw(gameWindow);

	string ammoString = to_string(gun->GetAmmo());//updating text on screen
	ammoText.setString("Ammo /  " + ammoString);
	gameWindow->draw(ammoText);

	string amountleft = to_string(wave1->getZombiesSize());
	zombiesLeft.setString("Zombies /  " + amountleft);
	gameWindow->draw(zombiesLeft);

	healthBar->Draw(gameWindow);
	int barSize = playerHealth * 3;
	//int barSize = 300 / playerHealth;
	healthBar->GetImage()->SetSourceRectX(barSize);

	healthBarOutline->Draw(gameWindow);

	for (size_t i = 0; i < ammoBox.size(); i++)
	{
		gameWindow->draw(ammoBox[i]->GetSprite());
	}
}
void ZombieGame::update(double elaspedTime, sf::Vector2f mousePos)
{
	fighter->FacePosition(mousePos);
	gun->Update(elaspedTime, mousePos);
	wave1->Update(elaspedTime, fighter->GetPosition());
	PlayerAndZombieCollision();
	gun->BulletCollision(wave1->GetZombieList(), &ammoBox);
	GetAmmo();
	
	int moveamount = 150 * elaspedTime;

	fighter->WASDmovementUpdate(moveamount);//call the charater class to handle WASD movements 
	mapCollisionTEST();

}

void ZombieGame::castEntites()
{
	for (size_t i = 0; i < this->entities.size(); i++)
	{
		if (this->entities[i]->GetName() == "BarInside")//casting the objects I've added to screen to my own instances of objects by getting the name i set
		{
			healthBar = dynamic_cast<Object*>(this->entities[i]); 
		}
		if (this->entities[i]->GetName() == "BarOutside")
		{
			healthBarOutline = dynamic_cast<Object*>(this->entities[i]); 
		}

		if (dynamic_cast<Player*>(this->entities[i]))//looking through all entities in the list and looking for the one that is of type player 
		{
			fighter = dynamic_cast<Player*>(this->entities[i]); 
		}
	}

}



void ZombieGame::SetupText()
{
	if (!MyFont.loadFromFile("Resources/Fonts/RockSalt-Regular.ttf"))///creating sf::Text for on screen text 
	{
		int test = 0;
		// Error...
	}
	ammoText.setFont(MyFont);
	string ammoString = to_string(gun->GetAmmo());
	int ScreenWidth = sf::VideoMode::getDesktopMode().width;
	ammoText.setString("Ammo /  " + ammoString);
	ammoText.setCharacterSize(23);
	ammoText.setPosition(sf::Vector2f(500, 40));

	zombiesLeft.setFont(MyFont);
	string amountleft = to_string(wave1->getZombiesSize());
	zombiesLeft.setString("Zombies /  " + amountleft);
	zombiesLeft.setCharacterSize(23);
	zombiesLeft.setPosition(sf::Vector2f(700, 40));
	zombiesLeft.setFillColor(sf::Color::Red);

}

void ZombieGame::GetAmmo()
{
	for (size_t i = 0; i < ammoBox.size(); i++)
	{
		if (fighter->DistanceCheckToPosition(ammoBox[i]->GetPostion(), 20))
		{
			gun->AddAmmo(15);
			ammoBox.erase(ammoBox.begin() + i);
			break;
		}
	}
}

void ZombieGame::PlayerAndZombieCollision()
{
	vector<Zombie*> newList = *wave1->GetZombieList();
	for (size_t i = 0; i < newList.size(); i++)
	{
		if (fighter->DistanceCheckToPosition(newList[i]->zombie->GetPosition(), 5))
		{
			//playerHealth -= 0.2;
		}
	}

	if (playerHealth < 0 || wave1->getZombiesSize() <= 0)//loading in maps 
	{
		playerHealth = 100;
		delete wave1;
		CreateNewGame();
		if (CurrentMap == 0)
		{
			map->LoadMap("Land&Sea.txt");//change map
			CurrentMap++;
		}
		else if (CurrentMap == 1)
		{
			map->LoadMap("LavaMap.txt");
			CurrentMap++;
		}
		else if (CurrentMap == 2)
		{
			map->LoadMap("LavaMap2.txt");
			CurrentMap++;
		}
		else if (CurrentMap == 3)
		{
			map->LoadMap("DemoMap.txt");
			CurrentMap = 0;
		}
	}
}


void ZombieGame::CreateNewGame()
{
	playerHealth = 100;
	wave1 = new Wave(200, 10, 10);
	gun = new GunManager(fighter);
}

void ZombieGame::mapCollisionTEST()
{
	//vector<Zombie*> zombieList = *wave1->GetZombieList();

	//for (size_t i = 0; i < zombieList.size(); i++)
	//{
	//	for (size_t j = 0; j < LAVATiles.size(); j++)
	//	{
	//		if (Collision(zombieList[i]->Position().x, zombieList[i]->Position().y, zombieList[i]->WidthHight().x, zombieList[i]->WidthHight().y,
	//			LAVATiles[j]->GetTileSprite()->GetPostion().x, LAVATiles[j]->GetTileSprite()->GetPostion().y, LAVATiles[j]->GetTileSprite()->GetSourceRect().x, LAVATiles[j]->GetTileSprite()->GetSourceRect().y))
	//		{
	//			zombieList[i]->SetPosition(zombieList[i]->Position());
	//			std::cout << "collision";
	//		}
	//	}
	//}
}
