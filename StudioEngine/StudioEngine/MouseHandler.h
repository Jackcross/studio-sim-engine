#pragma once
#include "Structs.h"
#include "Sprite.h"
class MouseHandler
{
public:

	MouseHandler();
	void Update(sf::Vector2f mousePos, sf::Event* event);
	void Draw(sf::RenderWindow* gameWindow);

	Sprite*				GetSprite() { return sprite; }
	sf::Vector2f		GetPosition() { return position; }
	void				SetPosition(sf::Vector2f pos) { position = pos; }
private:

	sf::Vector2f position;
	Sprite* sprite;
	sf::Event* event;

};

