#include "Player.h"
//Creator : Jack Cross 

Player::Player()
{
	_entityType = PLAYER_TYPE;
}

void Player::WASDmovementUpdate(int Speed)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::W))
	{
		AddPosition(sf::Vector2f(0, Speed * -1));
		// left key is pressed: move our character
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::S))
	{
		AddPosition(sf::Vector2f(0, Speed));//using AddPosition and not set position to increment the position
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		AddPosition(sf::Vector2f(Speed * -1, 0));
	}
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		AddPosition(sf::Vector2f(Speed, 0));
	}
}

void Player::Update()
{
}
