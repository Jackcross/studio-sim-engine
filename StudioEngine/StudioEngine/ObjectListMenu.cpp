#include "ObjectListMenu.h"

ObjectListMenu::ObjectListMenu()
{
	SetupLeftMenuShape();

	if (!MyFont.loadFromFile("Resources/Fonts/Roboto-MediumItalic.ttf"))
	{
		int test = 0;
		// Error...
	}
	MenuTitle.setFont(MyFont);
	MenuTitle.setString("Scene : Entity List" );
	MenuTitle.setCharacterSize(18);
	MenuTitle.setPosition(7,32);
	SelectedScene = 0;

}

void ObjectListMenu::Update(sf::Vector2f mousePos, ObjectsOnScreen* objectScreenClass)
{
	for (size_t i = 0; i < EntityButtonList.size(); i++)
	{
		EntityButtonList[i]->update(mousePos);
		if (EntityButtonList[i]->isPressed())
		{
			objectScreenClass->AddHighlightBoxToEntity(EntityButtonList[i]->GetButtonText());
			//SelectedScene = i + 1;
		}
	}
}

void ObjectListMenu::Draw(sf::RenderWindow* gameWindow)
{
	gameWindow->draw(*LeftsideMenu);
	gameWindow->draw(*SideMenuBoarder);
	for (size_t i = 0; i < EntityButtonList.size(); i++)
	{
		EntityButtonList[i]->Draw(gameWindow);
	}
	gameWindow->draw(MenuTitle);
}

void ObjectListMenu::ResizeToWindow(sf::RenderWindow* gameWindow)
{
}

void ObjectListMenu::ChangeScene(std::vector<Entity*> AllEntites)
{
	EntityButtonList.clear();

	float Ypos = LeftsideMenu->getPosition().y + 35;
	float Xpos = LeftsideMenu->getPosition().x + 15;
	for (size_t i = 0; i < AllEntites.size(); i++)
	{
		Button* newBut = new Button(Xpos, Ypos, 125, 20, &MyFont, 12, AllEntites[i]->GetName(), sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
		EntityButtonList.push_back(newBut);
		Ypos += 20;
	}
	SetupLeftMenuShape();
}

//void ObjectListMenu::GetScene(std::vector<std::vector<Entity*>> SceneList)
//{
//	float Ypos = LeftsideMenu->getPosition().y + 35;
//	float Xpos = LeftsideMenu->getPosition().x + 15;
//	for (size_t i = 0; i < SceneList.size(); i++)
//	{
//		Button* newBut = new Button(Xpos, Ypos, 125, 20, &MyFont, 12, "test", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
//		
//	}
//}

void ObjectListMenu::SetupLeftMenuShape()
{
	int size = 21;
	if (EntityButtonList.size() > 0)
		size *= EntityButtonList.size();

	int ScreenWidth = sf::VideoMode::getDesktopMode().width;
	int ScreenHeight = sf::VideoMode::getDesktopMode().height;

	LeftsideMenu = new sf::RectangleShape();//create new rectangle shape for side menu // ONLY FOR POSITIONING PURPOSES, not drawn
	LeftsideMenu->setSize(sf::Vector2f(155, 48 +size));
	LeftsideMenu->setPosition(sf::Vector2f(0, 30));
	LeftsideMenu->setFillColor(sf::Color(70, 70, 70, 200));

	SideMenuBoarder = new sf::RectangleShape();
	SideMenuBoarder->setSize(sf::Vector2f(LeftsideMenu->getGlobalBounds().width - 2, LeftsideMenu->getGlobalBounds().height));
	SideMenuBoarder->setPosition(sf::Vector2f(LeftsideMenu->getPosition().x , LeftsideMenu->getPosition().y));
	SideMenuBoarder->setOutlineColor(sf::Color(20, 20, 20, 200));
	SideMenuBoarder->setOutlineThickness(2);
	SideMenuBoarder->setFillColor(sf::Color(0, 0, 0, 0));
}
 