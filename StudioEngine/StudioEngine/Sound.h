#pragma once
//Creator : Liam Davy
#include "AudioSource.h"

class Sound
{
public: 
	Sound(std::string name, std::string filePath, float volume, bool isLoop);
	~Sound();
	std::string GetName();
	void SetName(std::string name);
	void SetFilePath(std::string path);
	float GetVolume();
	void SetVolume(float volume);
	bool GetIsLooping();
	void SetIsLooping(bool isLoop);
	AudioSource* GetAudioSource();

private:
	std::string _soundName = "name";
	AudioSource* _audioSource = new AudioSource("Resources/Sounds/pop.WAV");
	float _soundVolume = 1.0f;
	bool _isLoop = false;
};

