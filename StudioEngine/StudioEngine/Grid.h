#pragma once
#include "Structs.h"
class Grid
{
public:
	Grid(int SizeWidth, int SizeHeight, float screenWidth, float screenHight, int StartX, int StartY);
	void Update(sf::Vector2f mousePos);
	void Draw(sf::RenderWindow* gameWindow);

	void CreateLayout();
private:

	vector<vector<sf::RectangleShape>> _GridPosition;
	
	vector<vector<int>> _buttonStates;
	int _sizewidth;
	int _sizeheight;
	float _screenWidth;
	float _screenHeight;
	int _startPositonX;
	int _startPostiionY;
	float _amountX;
	float _amountY;

	bool Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2);


	sf::Color idleColor;
	sf::Color hoverColor;
	sf::Color PressedColor;
};

