#pragma once
#include "Structs.h"
#include "Button.h"
#include "Entity.h"
#include "ObjectsOnScreen.h"
class ObjectListMenu
{
public:
	ObjectListMenu();
	void			Update(sf::Vector2f mousePos, ObjectsOnScreen* objectScreenClass);
	void			Draw(sf::RenderWindow* gameWindow);
	void			ResizeToWindow(sf::RenderWindow* gameWindow);
	void			ChangeScene(std::vector<Entity*> AllEntites);
	void			GetScene(std::vector<std::vector<Entity*>> SceneList);
private:


	void			SetupLeftMenuShape();
	sf::RectangleShape* LeftsideMenu;
	sf::RectangleShape* SideMenuBoarder;
	std::vector<Button*> EntityButtonList;
		
	sf::Font MyFont;

	sf::Text MenuTitle;

	std::vector<std::vector<Entity*>> SceneList;
	std::vector<Button*> SceneButtonList;
	int SelectedScene;
};

