#pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "Button.h"
#include "Sprite.h"
#include "ObjectsOnScreen.h"
#include "TextBox.h"
class ImagesMenu
{
public:
	ImagesMenu(sf::Font* myFont, ObjectsOnScreen* ScreenObjects);
	void			Update(sf::Vector2f mousePos, sf::Event* event);
	void			Draw(sf::RenderWindow* gameWindow, vector<sf::RectangleShape*> LayoutShapes);
	void			DropORResetMouseSprite(vector<sf::RectangleShape*> LayoutShapes);

	void			ResizeWindow(sf::RenderWindow* gameWindow);

private:
	void			SetUpReferenceShaps();
	void			DrawHighBoxAndSetMouseSprite(sf::RenderWindow* gameWindow);
	void			Charaters();
	void			CreateCharaterHighltBox();
	void			CreateNewEntity(Sprite* newEntitySprite);

	bool			Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2);
	void			LoadImageMenuTextures();
	void			LoadGameSprites();

	void			ButtonSetup();

	LoadFromFile		fileLoader;

	sf::Event*			event;
	ObjectsOnScreen*	ScreenObjects;

	sf::Vector2f		mousePosition;
	Sprite*				mouseSprite;

	sf::Font*			myFont;
	vector<sf::RectangleShape> SelectCharaterBoxes;
	vector<Sprite*>		objectSprites;
	Button*				LoadImagebtn;

	sf::RectangleShape* sideMenu;

};

