#pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "Sprite.h"

enum List_state { OPEN, CLOSED };
class DropList
{
public:
	DropList(int amount, sf::Vector2f position, sf::Vector2f size);
	void			CreateList();

	void			Update(const sf::Vector2f mousePos);
	void			Draw(sf::RenderWindow* gameWindow);
	void			SetPosition(sf::Vector2f position);

	void			SetTextAt(int index, string text);
	void			SetAllText(vector<string> text);
	void			AddTextOption(string text);
	void			SetSelectedOption(string text);
	string			GetSelection() { return _ListText[0].getString(); }
	bool			CheckListChange() { return _changeBool; }
	void			AddIfNoneExistent(string name);

private:

	int						_listState;
	bool					_changeBool;
	sf::Font				_MyFont;
	vector<int>				_buttonState;
	sf::Vector2f			_size;
	int						_amount;
	sf::Vector2f			_position;

	vector<sf::RectangleShape*> _ListRec;
	vector<sf::Text>		_ListText;

	sf::RectangleShape*		_test;

	sf::Color				_idleColor;
	sf::Color				_hoverColor;
	sf::Color				_PressedColor;

	//sf::Texture DropDownArrowTexture;
	//sf::Sprite* DropDownArrowSprite;

	Sprite*					_DropDownArrowSprite;

};

