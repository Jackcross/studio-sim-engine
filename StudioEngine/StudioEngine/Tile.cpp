//Creator : Liam Davy
#include "Tile.h"

Tile::Tile(std::string spritePath)
{
	_tileSprite = new Sprite("Resources/MapTextures/" + spritePath);
	_tileSprite->SetFilePath(spritePath);
}

Tile::~Tile()
{
	delete _tileSprite;
	//delete& _location;
}

Sprite* Tile::GetTileSprite()
{
	return _tileSprite;
}

void Tile::SetTileSprite(Sprite* sprite)
{
	sf::Vector2f sourceRect = _tileSprite->GetSourceRect();
	_tileSprite->SetTexture(sprite->GetTexutre());
	_tileSprite->SetFilePath(sprite->GetFilePath());
	_tileSprite->SetSourceRect(sourceRect);
}

sf::Vector2i Tile::GetLocationInGrid()
{
	return _location;
}

void Tile::SetLocationInGrid(sf::Vector2i location)
{
	_location = location;
}
