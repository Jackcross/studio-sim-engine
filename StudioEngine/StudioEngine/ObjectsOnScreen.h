#pragma once
//Creator : Jack Cross 
#include "Structs.h"
#include "Entity.h"
#include "Player.h"
#include "NPC.h"
#include "Object.h"
#include "LoadFromFile.h"
#include "Button.h"
#include "UndoController.h"
enum Mouse_state{MOUSEPRESS, MOUSEHOLD, IDLE};


class ObjectsOnScreen
{
public:
	ObjectsOnScreen();
	
	void			Draw(sf::RenderWindow* gameWindow);
	void			Update(sf::Vector2f mousePos, sf::Event* event, float time);
	void			UpdateEntityMoveWithMouse( sf::Event* event);

	void			CreateNewEntity(Sprite* sprite);
	void			CreateNewPlayer(Sprite* playerSprite);
	void			CreateNewNPC(Sprite* NPCSprite);
	void			CreateNewObject(Sprite* ObjectSprite);
	void			CreateEntityBasedOnType(Entity* entity);

	void			SetStoreSprites(vector<Sprite*> sprites) { StoreSprites = sprites;  }
	void			SaveEntities(string path);
	void			LoadEntitiesFromFiles(string path);
	void			RemoveEntity(Entity* removeEntity);
	

	void			UpdateRightClick( Entity* entity, sf::Event* event);
	void			DrawRightClick(sf::RenderWindow* gameWindow);
	void			AddHighlightBoxToEntity(string name);

	void			AddEntity(Entity* newEntity) { AllEntites.push_back(newEntity); }
	void			AddEntity(Sprite* newEntity);

	Entity*			GetEntityAtIndex(int index) { return AllEntites.at(index); }
	vector<Entity*> GetAllOfType(Entity_Type type);

	vector<Entity*> GetAllEntities() { return AllEntites; }


	Entity*			returnEntitySelected() { return EntitySelected; }
	UndoController* UndoControl;


private:

	void			GroupSelection(sf::Vector2f mousePos, sf::Event* event);
	// different lists depending on Entity_Type  a new sprite added to screen will auto become a standard entity and added to that list 
	vector<Entity*> AllEntites;
	vector<Player*> PlayerEntities;
	vector<NPC*> NPCEntites;
	vector<Object*> ObjectEntites;

	Sprite* mouseHoldSprite;
	Mouse_state mouseState;


	Entity* EntitySelected;
	Sprite* SpriteSelected;

	Button* copybtn;
	Button* pastebtn;
	Button* deletebtn;
	bool rightClickOpenBool;
	Entity* copyEntity;
	Entity* HoldBeforeCopyEntity;


	vector<Sprite*>StoreSprites;

	sf::Event event;
	Sprite* mouseSprite;
	sf::Vector2f mousePosition;

	LoadFromFile fileLoader;
	//int SceneNumber;
	sf::Font MyFont;

	float mousePosOnImageX = 0;
	float mousePosOnImageY = 0;

	vector<Sprite*> groupSelection;
	vector<sf::RectangleShape> groupHightlight;
	vector<Entity*> highLightedEntities;
	int groupNumber = 0;

	int groupBoxState;
	float mouseHoldPosX = 0;
	float mouseHoldPosY = 0;
	sf::Vector2f entityUndoPositionHold;
	bool isMouseMoving;
	int undoEntityNum;


	sf::Vector2f oldPosition;
	Entity undoEntity;





};

