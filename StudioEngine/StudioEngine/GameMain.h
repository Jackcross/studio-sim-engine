#pragma once
#include "TileMap.h"
#include"ZombieGame.h"
class GameMain
{
public:
	GameMain(vector<Entity*> entites, TileMap* map);
	void Draw(sf::RenderWindow* gameWindow);
	void update(double elaspedTime, sf::Vector2f mousePos);

private:
	ZombieGame* newZombieGame;

};

