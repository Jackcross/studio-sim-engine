#pragma once
//Creator : Liam Davy   
#include "Tile.h"
#include "Renderer.h"
#include <list>
#include "LoadFromFile.h"
class TileMap
{
public:
	TileMap(int gridWidth, int gridHeight, float ScaleFactor);
	~TileMap();

	void			UpdateGrid();
	void			ChangeTileTextureAtGridLocation(sf::Vector2i gridLocation, Sprite* sprite);
	void			ChangeTileTextureAtPosition(sf::Vector2f Pos,Sprite* sprite);
	void			SaveMap(string path);
	void			LoadMap(string path);
	void			FitToScreen(sf::RenderWindow* window);
	sf::Vector2i	GetGridSize();
	void			SetGridSize(sf::Vector2i gridSize);
	sf::Vector2f	GetTileSize();
	void			setTileSize(sf::Vector2f tileSourceSize);
	void			DrawTileMap(sf::RenderWindow* window, sf::Event* event);
	Sprite*			GetSpriteAtLocation(sf::Vector2f mousePos);
	std::vector<Tile*> GetGrid() { return _grid; }
	void			ClearMapUsingSprite(Sprite* sprite);
	void			DrawHighlightBoxAroundSelected(sf::Vector2f Pos, Sprite* sprite);
	void			UpdateOldMapToNewPositions();

private:
	sf::Vector2i	_gridSize;
	std::vector<Tile*> _grid;
	sf::Vector2f	_tileSourceRectSize;
	float			_scaleFactor;
	std::string		_defaultTilePath = "dirtTile.PNG";
	LoadFromFile	fileloader;
	sf::Event*		_event;
	std::vector<sf::RectangleShape> _highLightedSqua;
	std::vector<Tile*> _highLightedTiles;
};

