#include "GameMain.h"

GameMain::GameMain(vector<Entity*> entities, TileMap* map)
{
	newZombieGame = new ZombieGame(entities, map);
}

void GameMain::Draw(sf::RenderWindow* gameWindow) 
{
	newZombieGame->Draw(gameWindow);
}
void GameMain::update(double elaspedTime, sf::Vector2f mousePos)
{
	newZombieGame->update(elaspedTime, mousePos);
}
