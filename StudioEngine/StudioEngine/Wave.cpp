#include "Wave.h"

Wave::Wave(int slowZombies, int fastZombies, int heavyZombies)
{

	for (size_t i = 0; i < slowZombies; i++)
	{
		Sprite* slowZombieSprite = new Sprite("Resources/Textures/zombie_normal.png");

		Zombie* slowZombie = new Zombie(slowZombieSprite, 0.6, 1, 10);
		this->zombies.push_back(slowZombie);
	}

	for (size_t i = 0; i < fastZombies; i++)
	{
		Sprite* fastZombieSprite = new Sprite("Resources/Textures/zombie_fast.png");

		Zombie* fastZombies = new Zombie(fastZombieSprite, 1.3, 1, 8);
		this->zombies.push_back(fastZombies);
	}

	for (size_t i = 0; i < heavyZombies; i++)
	{
		Sprite* heavyZombieSprite = new Sprite("Resources/Textures/zombie_fat.png");

		Zombie* heavyZombies = new Zombie(heavyZombieSprite, 0.3, 3, 2);
		this->zombies.push_back(heavyZombies);
	}

	for (size_t i = 0; i < zombies.size(); i++)
	{
		NPCzombies.push_back(zombies[i]->zombie);
	}
}

Wave::~Wave()
{
	for (size_t i = 0; i < zombies.size(); i++)
	{
		delete zombies[i];
	}
	zombies.clear();
}

void Wave::Update(double elaspedTime, sf::Vector2f trackPosition)
{
	for (size_t i = 0; i < zombies.size(); i++)
	{
		zombies[i]->Update(elaspedTime, trackPosition,NPCzombies);
	}
}

void Wave::Draw(sf::RenderWindow* gameWindow)
{
	for (size_t i = 0; i < zombies.size(); i++)
	{
		zombies[i]->Draw(gameWindow);
	}
}
