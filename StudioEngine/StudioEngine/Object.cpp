#include "Object.h"
//Creator : Jack Cross 

Object::Object()
{
	_movement = new Movement();
}
bool Object::DistanceCheck(Entity* otherEntity, float distance)
{
	return _movement->DistanceCheck(otherEntity, this, distance);
}