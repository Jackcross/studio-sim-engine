#pragma once
//Creator : Jack Cross 

#include "Structs.h"
#include "Sprite.h"
class Entity
{
public:
	Entity();

	virtual void		Update();
	virtual void		Draw(sf::RenderWindow* window);
	void				SetPosition(sf::Vector2f pos) { _image->SetPosition(pos); }
	sf::Vector2f		GetPosition() { return _image->GetPostion(); }
	float				GetRotation() { return _image->GetRotation(); }
	void				AddPosition(sf::Vector2f position) { SetPosition(sf::Vector2f(GetPosition().x + position.x, GetPosition().y + position.y)); }
	void				SetCollidable(bool collid) { _isCollidable = collid; }
	bool				GetCoolidable() { return _isCollidable; }

	void				SetSprite(Sprite* sprite) { _image = sprite; }


	Sprite*				GetImage() { return _image; }

	Entity_Type			GetType() {return _entityType; }
	void				SetType(Entity_Type type) { _entityType = type; }

	void				SetName(string name) { this->_name = name; }
	string				GetName() { return _name; }

protected:
		
	bool			_isCollidable;
	Sprite*			_image;
	Entity_Type		_entityType;
	string			_name = "n/a";
};

