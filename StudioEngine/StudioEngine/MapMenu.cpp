#include "MapMenu.h"

MapMenu::MapMenu(TileMap* map, sf::Font* font)
{
	this->_map = map;
	this->font = font;
	SetupReferencePositions();

	DisplayedSpriteText.setFont(*font);
	DisplayedSpriteText.setCharacterSize(18);
	DisplayedSpriteText.setString("Seleted Sprite:");
	DisplayedSpriteText.setPosition(sf::Vector2f(_sideMenu->getPosition().x + 20, 100));

	DisplayedSpriteNAME.setFont(*font);
	DisplayedSpriteNAME.setCharacterSize(18);
	DisplayedSpriteNAME.setString(" ");
	DisplayedSpriteNAME.setPosition(sf::Vector2f(DisplayedSpriteText.getPosition().x + DisplayedSpriteText.getGlobalBounds().width, 100));



	ButtonSetup();
	event = new sf::Event();
	TextboxSetup();
	listSetup();

	LoadGameSprite();//Call the fileloader funtion to file-load-in sprites into a vector
	SetupSprites();
}

void MapMenu::Draw(sf::RenderWindow* gameWindow, vector<sf::RectangleShape*> LayoutShapes)
{
	if (mouseSprite != nullptr)
	{
		mouseSprite->SetPosition(mousePosition);
		gameWindow->draw(mouseSprite->GetSprite());
	}
	DropORResetMouseSprite(LayoutShapes);//set texture rect to zero if button released and drop sprite onto screen space

	DrawHighBoxAndSetMouseSprite(gameWindow);

	for (size_t i = 0; i < _VtilesSprites.size(); i++)
	{
		gameWindow->draw(_VtilesSprites[i]->GetSprite());
	}

	gameWindow->draw(DisplaySelected->GetSprite());
	gameWindow->draw(DisplayedSpriteNAME);
	gameWindow->draw(DisplayedSpriteText);
	savebtn->Draw(gameWindow);
	Clearbtn->Draw(gameWindow);
	saveTextBox->Draw(gameWindow);
	LoadList->Draw(gameWindow);
	_isWallList->Draw(gameWindow);
	LoadNewSpritesbtn->Draw(gameWindow);
}

void MapMenu::Update(sf::Vector2f mousePos, sf::Event* event)
{
	this->event = event;
	mousePosition = mousePos;
	savebtn->update(mousePos);
	Clearbtn->update(mousePos);
	LoadNewSpritesbtn->update(mousePos);

	if (savebtn->isPressed() && saveTextBox->GetInputString().getString() != "")
	{
		_map->SaveMap(saveTextBox->GetInputString().getString());
		LoadList->AddIfNoneExistent(saveTextBox->GetInputString().getString());
		LoadList->SetSelectedOption(saveTextBox->GetInputString().getString());
	}
	if (Clearbtn->isPressed())
	{
		_map->ClearMapUsingSprite(_clearSprite);
	}
	if (LoadNewSpritesbtn->isPressed())
	{
		LoadGameSprite();
		SetupSprites();
	}

	saveTextBox->Update(mousePos,event);
	LoadList->Update(mousePos);
	_isWallList->Update(mousePos);

	if (LoadList->CheckListChange() == true)
	{
		_map->LoadMap(LoadList->GetSelection() + ".txt");
		string path = LoadList->GetSelection();
		saveTextBox->SetString(path);
	}
}

void MapMenu::DrawClosed(sf::RenderWindow* gameWindow, vector<sf::RectangleShape*> LayoutShapes)//'Draw' meaning, keep drawing the MAP while the assett window is closed.(keep updating when the asset window is closed)
{
	if (mouseSprite != nullptr)
	{
		mouseSprite->SetPosition(mousePosition);
		gameWindow->draw(mouseSprite->GetSprite());
	}

	if (mouseSprite->GetSourceRect() != sf::Vector2f(0, 0))// if the user is dragging a new entity to screen space and then realeases it, create a new entity by calling createNewEntity
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu
			if (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left) && (mouseSprite->GetSourceRect() != sf::Vector2f(0, 0)))
			{
				Sleep(50);
				_map->ChangeTileTextureAtPosition(mousePosition, mouseSprite);
			}
		}
	}
	if ((sf::Mouse::isButtonPressed(sf::Mouse::Left)))
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu

			if ((sf::Mouse::isButtonPressed(sf::Mouse::Left)) && (DisplaySelected->GetSourceRect() != sf::Vector2f(0, 0)) && (mouseSprite->GetSourceRect() == sf::Vector2f(0, 0)))
			{
				_map->ChangeTileTextureAtPosition(mousePosition, DisplaySelected);
			}
		}
	}
	if ((sf::Mouse::isButtonPressed(sf::Mouse::Right)))
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu

			if ((sf::Mouse::isButtonPressed(sf::Mouse::Right)) && (mouseSprite->GetSourceRect() == sf::Vector2f(0, 0)))
			{
				_map->ChangeTileTextureAtPosition(mousePosition, _clearSprite);
			}
		}
	}


	//if user released mouse click then 
	if (mouseSprite->GetSourceRect().x > 1 && (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left)))//make sure the Sprite attached to the mouse is not showing once mouse is released 
	{
		mouseSprite->SetSourceRect(0.0f, 0.0f);
	}

}

void MapMenu::UpdateClosed(sf::Vector2f mousePos, sf::Event* event)
{
	this->event = event;
	mousePosition = mousePos;
}

void MapMenu::SetupReferencePositions()
{
	ScreenWidth = sf::VideoMode::getDesktopMode().width;
	ScreenHeight = sf::VideoMode::getDesktopMode().height;

	_sideMenu = new sf::RectangleShape();//create new rectangle shape for side menu // ONLY FOR POSITIONING PURPOSES, not drawn
	_sideMenu->setSize(sf::Vector2f(400, 1080));
	_sideMenu->setPosition(sf::Vector2f(ScreenWidth - 400, 30));

	_topMenu = new sf::RectangleShape();//create new rectangle shape for Top menu // ONLY FOR POSITIONING PURPOSES, not drawn
	_topMenu->setSize(sf::Vector2f(1920, 30));
	_topMenu->setPosition(sf::Vector2f(0, 0));
	_topMenu->setFillColor(sf::Color(150, 150, 150, 200));
}

void MapMenu::LoadGameSprite()
{
	_VtilesSprites = _fileLoader.LoadMapSprites();
}

void MapMenu::SetupSprites()
{
	mouseSprite = new Sprite("Resources/EngineTextures/DownArrow.png", sf::Vector2f(75, 100));
	mouseSprite->SetSourceRect(sf::Vector2f(0, 0));
	
	_selectSprite = new Sprite("Resources/EngineTextures/SelectTile.png");
	_VtilesSprites.push_back(_selectSprite);

	_clearSprite = new Sprite("Resources/EngineTextures/RemoveTile.png");

	if (DisplaySelected == nullptr)
	{
		DisplaySelected = new Sprite("Resources/EngineTextures/DownArrow.png", sf::Vector2f(0, 0));
		DisplaySelected->SetSourceRect(sf::Vector2f(0, 0));
		DisplaySelected->SetPosition((_sideMenu->getPosition().x + (_sideMenu->getGlobalBounds().width / 2)) - DisplaySelected->GetWidth(), 140);
	}

	int CalcEdgeSpace = (400 * 5) / 100;
	sf::Vector2f area(_sideMenu->getGlobalBounds().width - CalcEdgeSpace, 800);

	int XscreenSpace = _sideMenu->getPosition().x;
	int x = 0;
	int y = 270;
	int BiggestHeight = 0;
	for (size_t i = 0; i < _VtilesSprites.size(); i++)
	{
		if (_VtilesSprites[i]->GetSprite().getLocalBounds().height > BiggestHeight)//get the tallest sprite on this line
		{
			BiggestHeight = _VtilesSprites[i]->GetSprite().getGlobalBounds().height;
		}
		if ((_VtilesSprites[i]->GetSprite().getGlobalBounds().width + x) > area.x)// if theres no space left on the line, go to the next. 
		{
			y += BiggestHeight;//the next line will start with a Y difference of the largest sprite from the previous line 
			x = 0;
			BiggestHeight = 0;
		}

		_VtilesSprites[i]->SetPosition((_sideMenu->getPosition().x + CalcEdgeSpace) + x, y);//set the position 
		x += _VtilesSprites[i]->GetSprite().getGlobalBounds().width;// add width of current sprite to x, so that the next sprite spawns in the next position;
	}
}

void MapMenu::DrawHighBoxAndSetMouseSprite(sf::RenderWindow* gameWindow)
{
	for (size_t i = 0; i < _VtilesSprites.size(); i++)
	{
		if (_VtilesSprites[i]->GetSprite().getGlobalBounds().contains(mousePosition) && _sideMenu->getGlobalBounds().contains(mousePosition)  && mouseSprite->GetSourceRect() == sf::Vector2f(0, 0))//check if mouse is hovering over sprites from menu
		{
			if (event->type == sf::Event::MouseButtonPressed && (event->mouseButton.button == sf::Mouse::Left))//if the user clicks on sprite, set the mousesprite to the clicked on sprite 
			{		//set mouse texture
				sf::Texture tex = _VtilesSprites[i]->GetTexutre();
				mouseSprite->SetTexture(tex);
				mouseSprite->SetFilePath(_VtilesSprites[i]->GetFilePath());
				mouseSprite->SetSourceRect(_VtilesSprites[i]->GetSourceRect());

				DisplaySelected->SetTexture(tex);
				DisplaySelected->SetSourceRect(_VtilesSprites[i]->GetSourceRect());
				DisplaySelected->SetFilePath(_VtilesSprites[i]->GetFilePath());
				DisplaySelected->SetPosition((_sideMenu->getPosition().x + (_sideMenu->getGlobalBounds().width / 2)) - (DisplaySelected->GetWidth() /2), 140);
				DisplayedSpriteNAME.setString(DisplaySelected->GetFilePath());

			}
		}
	}
}

void MapMenu::DropORResetMouseSprite(vector<sf::RectangleShape*> LayoutShapes)
{
	if (mouseSprite->GetSourceRect() != sf::Vector2f(0, 0))// if the user is dragging a new entity to screen space and then realeases it, create a new entity by calling createNewEntity
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu
			if (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left)  && (mouseSprite->GetSourceRect() != sf::Vector2f(0, 0)) && (Collision(mousePosition.x, mousePosition.y, 1, mouseSprite->GetSourceRect().y, LayoutShapes[i]->getPosition().x, LayoutShapes[i]->getPosition().y, LayoutShapes[i]->getGlobalBounds().width, LayoutShapes[i]->getGlobalBounds().height) == false))
			{
				Sleep(50);
				_map->ChangeTileTextureAtPosition(mousePosition, mouseSprite);
			}
		}
	}
	if ((sf::Mouse::isButtonPressed(sf::Mouse::Left)))
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu

			if ((sf::Mouse::isButtonPressed(sf::Mouse::Left)) && (DisplaySelected->GetSourceRect() != sf::Vector2f(0, 0)) && (mouseSprite->GetSourceRect() == sf::Vector2f(0, 0)) && (Collision(mouseSprite->GetPostion().x, mouseSprite->GetPostion().y, mouseSprite->GetSourceRect().x, mouseSprite->GetSourceRect().y, LayoutShapes[i]->getPosition().x, LayoutShapes[i]->getPosition().y, LayoutShapes[i]->getGlobalBounds().width, LayoutShapes[i]->getGlobalBounds().height) == false))
			{
				_map->ChangeTileTextureAtPosition(mousePosition, DisplaySelected);
			}
		}
	}
	if ((sf::Mouse::isButtonPressed(sf::Mouse::Right)))
	{
		for (size_t i = 0; i < LayoutShapes.size(); i++)
		{//Check for position of sprite. dropping it not on menu

			if ((sf::Mouse::isButtonPressed(sf::Mouse::Right)) && (mouseSprite->GetSourceRect() == sf::Vector2f(0, 0)) && (Collision(mouseSprite->GetPostion().x, mouseSprite->GetPostion().y, mouseSprite->GetSourceRect().x, mouseSprite->GetSourceRect().y, LayoutShapes[i]->getPosition().x, LayoutShapes[i]->getPosition().y, LayoutShapes[i]->getGlobalBounds().width, LayoutShapes[i]->getGlobalBounds().height) == false))
			{
				_map->ChangeTileTextureAtPosition(mousePosition, _clearSprite);
			}
		}
	}

	//if user released mouse click then 
	if (mouseSprite->GetSourceRect().x > 1 && (event->type == sf::Event::MouseButtonReleased && (event->mouseButton.button == sf::Mouse::Left)))//make sure the Sprite attached to the mouse is not showing once mouse is released 
	{
		mouseSprite->SetSourceRect(0.0f, 0.0f);
	}
}

bool MapMenu::Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2)
{
	if (x1 < x2 + width2 &&
		x1 + width1 > x2&&
		y1 < y2 + hight2 &&
		y1 + hight1 > y2) {
		return true;
	}
	else
	{
		return false;
	}
}

void MapMenu::listSetup()
{
	LoadList = new DropList(0, sf::Vector2f(_sideMenu->getPosition().x +75, _sideMenu->getPosition().y + 45), sf::Vector2f(120, 22));
	LoadListUpdate();

	//Is drawn, but not currently in use
	_isWallList = new DropList(2, sf::Vector2f(_sideMenu->getPosition().x + 17, _sideMenu->getPosition().y + 100), sf::Vector2f(60, 22));
	_isWallList->AddTextOption("YES");
	_isWallList->AddTextOption("NO");
	_isWallList->SetSelectedOption("NO");
	
}

void MapMenu::ButtonSetup()
{
	savebtn = new Button((int)_sideMenu->getPosition().x + 335, (int)_sideMenu->getPosition().y + 45, 50, 22, font, 17, "Save", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	Clearbtn = new Button((int)_sideMenu->getPosition().x + 15, (int)_sideMenu->getPosition().y + 45, 50, 25, font, 15, "Clear", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	LoadNewSpritesbtn = new Button((int)_sideMenu->getPosition().x + 295, (int)_sideMenu->getPosition().y + 70, 90, 25, font, 15, "LoadSprites", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
}

void MapMenu::TextboxSetup()
{
	saveTextBox = new TextBox(_sideMenu->getPosition().x + 230, _sideMenu->getPosition().y + 45, 100, 22, font, "", false, event, "");
}

void MapMenu::LoadListUpdate()
{
	vector<string> maps;
	maps = _fileLoader.GetMapFilePaths();

	for (size_t i = 0; i < maps.size(); i++)
	{
		string path = maps[i];
		path = RemoveTxtFromString(path);
		LoadList->AddTextOption(path);
	}
	string path = LoadList->GetSelection();
	saveTextBox->SetString(path);
	if (maps.size() > 0)
	{
		_map->LoadMap(path + ".txt");
	}
}


string MapMenu::RemoveTxtFromString(string Text)
{
	Text.pop_back();
	Text.pop_back();
	Text.pop_back();
	Text.pop_back();
	return Text;
}

void MapMenu::WindowResize(sf::RenderWindow* gameWindow)
{

	_sideMenu->setPosition(sf::Vector2f(gameWindow->getSize().x - 400, 30));
	LoadList->SetPosition(sf::Vector2f(_sideMenu->getPosition().x + 75, _sideMenu->getPosition().y + 45));
	DisplaySelected->SetPosition((_sideMenu->getPosition().x + (_sideMenu->getGlobalBounds().width / 2)) - (DisplaySelected->GetWidth() / 2), 140);
	saveTextBox->SetPosition(sf::Vector2f(_sideMenu->getPosition().x + 230, _sideMenu->getPosition().y+ 45));
	savebtn->SetPosition(sf::Vector2f((int)_sideMenu->getPosition().x + 335, (int)_sideMenu->getPosition().y + 45));
	Clearbtn->SetPosition(sf::Vector2f((int)_sideMenu->getPosition().x + 15, (int)_sideMenu->getPosition().y + 45));
	LoadNewSpritesbtn->SetPosition(sf::Vector2f((int)_sideMenu->getPosition().x + 295, (int)_sideMenu->getPosition().y + 70));
	DisplayedSpriteText.setPosition(sf::Vector2f(_sideMenu->getPosition().x + 20, 100));
	DisplayedSpriteNAME.setPosition(sf::Vector2f(DisplayedSpriteText.getPosition().x + DisplayedSpriteText.getGlobalBounds().width, 100));


	int CalcEdgeSpace = (400 * 5) / 100;
	sf::Vector2f area(_sideMenu->getGlobalBounds().width - CalcEdgeSpace, 800);

	int XscreenSpace = _sideMenu->getPosition().x;
	int x = 0;
	int y = 270;
	int BiggestHeight = 0;
	for (size_t i = 0; i < _VtilesSprites.size(); i++)
	{
		if (_VtilesSprites[i]->GetSprite().getLocalBounds().height > BiggestHeight)//get the tallest sprite on this line
		{
			BiggestHeight = _VtilesSprites[i]->GetSprite().getGlobalBounds().height;
		}
		if ((_VtilesSprites[i]->GetSprite().getGlobalBounds().width + x) > area.x)// if theres no space left on the line, go to the next. 
		{
			y += BiggestHeight;//the next line will start with a Y difference of the largest sprite from the previous line 
			x = 0;
			BiggestHeight = 0;
		}

		_VtilesSprites[i]->SetPosition((_sideMenu->getPosition().x + CalcEdgeSpace) + x, y);//set the position 
		x += _VtilesSprites[i]->GetSprite().getGlobalBounds().width;// add width of current sprite to x, so that the next sprite spawns in the next position;
	}
}


