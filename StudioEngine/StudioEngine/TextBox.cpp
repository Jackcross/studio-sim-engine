#include "TextBox.h"
#include <Windows.h>
#include <iomanip>
#include <sstream>
//Creator : Jack Cross 

TextBox::TextBox(float posX, float posY, float width, float height, sf::Font* myFont, string text, bool ButtonBool, sf::Event* event, string ButtonText = "")
{
	_TextAmount = 11;
	_AddAmount = 0.5;
	_storedValue = 0;
	_myFont = myFont;
	_position.x = posX;
	_position.y = posY;
	_size.x = width;
	_size.y = height;
	_text.setString(text);
	_text.setCharacterSize(15);
	_text.setFont(*myFont);
	_text.setFillColor(sf::Color(0, 0, 0, 255));
	_buttonBool = ButtonBool;
	_buttonString = ButtonText;
	_Event = event;
	_box.setPosition(_position);
	_box.setSize(_size);
	_box.setFillColor(sf::Color::White);
	_text.setPosition(_box.getPosition().x + 5, _box.getPosition().y + 2);
	
	if (_buttonBool == true && ButtonText == "")
	{
		_buttonLeft = new Button(_position.x + _box.getGlobalBounds().width + 4, _position.y, 18, _box.getGlobalBounds().height, _myFont, 12, "<", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
		_buttonRight = new Button(_position.x + _box.getGlobalBounds().width + _buttonLeft->getRec()->getGlobalBounds().width + 8, _position.y, 18, _box.getGlobalBounds().height, _myFont, 12, ">", sf::Color(70, 70, 70, 255), sf::Color(150, 150, 150, 200), sf::Color(20, 20, 20, 200));
	}

}

void TextBox::Draw(sf::RenderWindow* gameWindow)
{
	gameWindow->draw(_box);

	if (_buttonBool == true)
	{
		//_button->Draw(gameWindow);
		_buttonLeft->Draw(gameWindow);
		_buttonRight->Draw(gameWindow);
	}
	gameWindow->draw(_text);
}

void TextBox::Update(sf::Vector2f mousePos, sf::Event* event)
{
	_Event = event;
	if (_buttonBool == true)
	{
		_buttonLeft->update(mousePos);
		_buttonRight->update(mousePos);

		if (_buttonLeft->isPressed() == true && _text.getString() != "n/a")
		{
			Sleep(100);
			_storedValue -= _AddAmount;
			SetFloat(_storedValue);
		}

		if (_buttonRight->isPressed() == true && _text.getString() != "n/a")
		{
			Sleep(100);
			_storedValue += _AddAmount;
			SetFloat(_storedValue);
		}
	}


	if (_box.getGlobalBounds().contains(mousePos))
	{
		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			_isSelected = true;
		}
	}
	else if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
	{
		_isSelected = false;
	}



	if (_isSelected == true)
	{
		if (event->type == sf::Event::TextEntered)
		{
			if (event->text.unicode < 128 && event->text.unicode != 8 && sf::Keyboard::isKeyPressed(sf::Keyboard::Enter) == false)
			{
				if (_inputHold.size() < _TextAmount)
				{
					_inputHold += event->text.unicode;
					Sleep(100);

					_text.setString(_inputHold);
				}
			}
			if (event->text.unicode == 8 && _inputHold.size() > 0)
			{
				_inputHold.pop_back();
				Sleep(100);
				_text.setString(_inputHold);

			}
		}
	}

	CheckSelected();
}

bool TextBox::CheckSelected()
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Enter))
	{
		return true;
	}
    if (_buttonBool == true && (_buttonLeft->isPressed() == true || (_buttonRight->isPressed() == true) && _text.getString() != "n/a"))
	{
		return true;
	}
	else
	{
		return false;
	}

}

void TextBox::SetFloat(float x)
{
	_storedValue = x;
	_text.setString(FloatToString(x)); 
}

void TextBox::ChangeBTNType(Button_Type type)///not working
{
	_BTNType = type; 
}

void TextBox::SetPosition(sf::Vector2f pos)
{
	_position = pos;
	_box.setPosition(pos);
	_text.setPosition(_box.getPosition().x + 5, _box.getPosition().y + 2);
	if (_buttonBool == true)
	{
		_buttonLeft->SetPosition(sf::Vector2f(_position.x + _box.getGlobalBounds().width + 4, _position.y));
		_buttonRight->SetPosition(sf::Vector2f(_position.x + _box.getGlobalBounds().width + _buttonLeft->getRec()->getGlobalBounds().width + 8, _position.y));
	}

}

Button_side TextBox::GetArrowPress()
{
	if (_buttonBool == true) 
	{
		if (_buttonLeft->isPressed() == true)
		{
			return BTN_LEFT;
		}

		if (_buttonRight->isPressed() == true)
		{
			return BTN_RIGHT;
		}
	}
}

string TextBox::FloatToString(float num)
{
	stringstream stream;
	stream << std::fixed << std::setprecision(1) << num;
	std::string s = stream.str();
	return s;
}
