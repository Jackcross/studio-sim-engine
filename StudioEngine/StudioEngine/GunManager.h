#pragma once
#include "Zombie.h"
#include "AudioManager.h"
struct bullet {

	sf::Vector2f position;
	sf::Vector2f angle;
	sf::Vector2f direction;
	float distance;
	Sprite* image;
};
class GunManager
{
public:

	GunManager(Entity* holder);
	void Draw(sf::RenderWindow* GameWindow);
	void Update(double elaspedTime, sf::Vector2f trackPosition);
	void CreateBullet(sf::Vector2f trackPosition);
	void BulletCollision(vector<Zombie*>* zombies, vector<Sprite*>* ammoBox);
	int	 GetAmmo() { return ammo; }
	void AddAmmo(int ammo) { this->ammo += ammo; }

private:


	float timeSinceLastBullet;

	float speed = 0.5;
	int damage = 1;
	vector<bullet> bullets;
	int ammo;
	sf::Clock clock;
	float time;
	Entity* player;
	AudioManager* audioManager;


};

