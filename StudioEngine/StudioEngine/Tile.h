#pragma once
//Creator : Liam Davy
#include "Sprite.h"
class Tile
{
public:
	Tile(std::string spritePath);
	~Tile();

	Sprite* GetTileSprite();
	void SetTileSprite(Sprite* sprite);
	sf::Vector2i GetLocationInGrid();
	void SetLocationInGrid(sf::Vector2i location);


private: 
	Sprite* _tileSprite;
	sf::Vector2i _location;
	bool hardWall;
};

