#pragma once
//Creator : Jack Cross 

#include "Entity.h"
#include "Movement.h"
class Object : public Entity
{
public:
	Object();
	void	AddPosition(sf::Vector2f position) { SetPosition(sf::Vector2f(GetPosition().x + position.x, GetPosition().y + position.y)); }
	void	SetSpeed(float speed) { _movement->SetSpeed(speed); }
	bool	DistanceCheck(Entity* otherEntity, float distance);
	void	Seek(float time, sf::Vector2f Target) { SetPosition(_movement->Seek( Target)); }
	void	MoveInDirection(float time, sf::Vector2f Target) { SetPosition(_movement->Seek(Target)); }

private:
	Movement* _movement;
};

