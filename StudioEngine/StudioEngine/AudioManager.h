#pragma once
//Creator : Liam Davy
#include "Sound.h"
#include <list>
class AudioManager
{
public:
	AudioManager();
	~AudioManager();

	void AddSoundToList(std::string name, std::string filePath, float volume, bool isLoop);
	void RemoveSoundFromList(std::string name);
	void PlaySounds(std::string soundName);
	void StopSound(std::string soundName);
	bool CheckIfPlaying(std::string soundName);
	void PlayMusic(std::string path);


private:
	std::list<Sound*> _gameSounds;
};

