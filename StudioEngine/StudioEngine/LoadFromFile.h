#pragma once
//Creator : Jack Cross
#include <iostream>
#include <sstream>
#include <fstream>
#include "Structs.h"
#include "Entity.h"
#include "Player.h"
#include "Object.h"
#include "NPC.h"
#include "Tile.h"



class LoadFromFile
{
public:
	LoadFromFile();
	
	static void OutputEntities(string path, vector<Entity*>Entites, vector<Sprite*> sprites);

	void MakeHeaderFile(vector<Entity*> Entites);
	static vector<Sprite*> GetSprites();

	void GetEntities( vector<Entity*>* undefinedEntities, vector<Player*>* Players, vector<NPC*>* NPCs, vector<Object*>* objects, string path);
	void GetEntities(vector<Entity*>* undefinedEntities, string path);

	static vector<Sprite*> LoadMapSprites();

	static void OutputMap(vector<Tile*> gird, string path);
	static vector<Tile*> GetMap(string path);
	static vector<string> GetMapFilePaths();
	static vector<string> GetEntityFilePaths();
private:

	static vector<Tile*> grid;
};

