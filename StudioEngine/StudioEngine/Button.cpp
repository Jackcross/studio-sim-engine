#include "Button.h"
//Creator : Jack Cross 

Button::Button(float x, float y, float width, float height, sf::Font* font, int fontSize, string text, sf::Color idleColor, sf::Color hoverColor, sf::Color activeColor)
{
	_Menustat = NA;
	_shape.setPosition(sf::Vector2f(x, y));
	_shape.setSize(sf::Vector2f(width, height));//set position for rectangle shape 

	_buttonState = BTN_IDLE;

	//setup for button text 
	this->_font = font;
	this->_text.setFont(*this->_font);
	this->_text.setString(text);
	this->_text.setCharacterSize(fontSize);
	this->_text.setPosition(_shape.getPosition().x +(_shape.getGlobalBounds().width  / 2.f)  - this->_text.getGlobalBounds().width / 2.f, (_shape.getPosition().y + (_shape.getGlobalBounds().height / 2.f) - this->_text.getGlobalBounds().height / 2.f) - 3);

	this->_idleColor = idleColor;//set the colours
	this->_hoverColor = hoverColor;
	this->_PressedColor = activeColor;

	this->_shape.setFillColor(this->_idleColor);
}

void Button::Draw(sf::RenderWindow* window)
{
	window->draw(_shape);

	window->draw(_text);
}

void Button::update(const sf::Vector2f mousePos)//button state is always idle until mouse touches button rect 
{
	_buttonState = BTN_IDLE;

	if (this->_shape.getGlobalBounds().contains(mousePos))
	{
		_buttonState = BTN_HOVER;

		if (sf::Mouse::isButtonPressed(sf::Mouse::Left))
		{
			_buttonState = BTN_PRESSED;
		}
	}

	switch (_buttonState)
	{
	case BTN_IDLE:
		_shape.setFillColor(_idleColor);
		break;
	case BTN_HOVER:
		_shape.setFillColor(_hoverColor);
		break;
	case BTN_PRESSED:
		_shape.setFillColor(_PressedColor);
		break;
	}
}

bool Button::isPressed()
{
	if (_buttonState == BTN_PRESSED)
	{
		return true;
	}
	return false;
}

void Button::SetPosition(sf::Vector2f pos)
{
	_shape.setPosition(pos);
	this->_text.setPosition(_shape.getPosition().x + (_shape.getGlobalBounds().width / 2.f) - this->_text.getGlobalBounds().width / 2.f, (_shape.getPosition().y + (_shape.getGlobalBounds().height / 2.f) - this->_text.getGlobalBounds().height / 2.f) - 3);
}
