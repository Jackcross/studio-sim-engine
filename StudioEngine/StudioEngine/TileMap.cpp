#include "TileMap.h"

TileMap::TileMap(int gridWidth, int gridHeight,float scaleFactor)
{
	_gridSize.x = gridWidth;
	_gridSize.y = gridHeight;
	_scaleFactor = scaleFactor;
	UpdateGrid();
}

TileMap::~TileMap()
{
}

void TileMap::UpdateGrid()
{
	_grid.clear();
	int y = 0;
	int x = 0;
	for (int j = 0; j < _gridSize.x; j++)
	{
		y = 49;
		for (int i = 0; i < _gridSize.y; i++)
		{
			Tile* tile = new Tile(_defaultTilePath);
			tile->GetTileSprite()->SetScale(_scaleFactor);
			tile->SetLocationInGrid(sf::Vector2i(j, i));
			tile->GetTileSprite()->SetPosition(x , y);
			_grid.push_back(tile);
			y += tile->GetTileSprite()->GetSprite().getGlobalBounds().height;
		}
		x += _grid[0]->GetTileSprite()->GetSprite().getGlobalBounds().width;
	}
}

void TileMap::ChangeTileTextureAtGridLocation(sf::Vector2i gridLocation, Sprite* sprite)
{
	for (Tile* tile : _grid)
	{
		if (tile->GetLocationInGrid() == gridLocation)
		{
			tile->SetTileSprite(sprite);
			break;
		}
	}
}

void TileMap::ChangeTileTextureAtPosition(sf::Vector2f Pos, Sprite* sprite)
{
	if (sprite->GetFilePath() == "SelectTile.png")
	{
		DrawHighlightBoxAroundSelected(Pos, sprite);
	}
	else
	{
		for (size_t i = 0; i < _grid.size(); i++)
		{
			if (_grid[i]->GetTileSprite()->GetSprite().getGlobalBounds().contains(Pos))
			{
				_grid[i]->SetTileSprite(sprite);
				break;
			}
		}
	}
}

void TileMap::SaveMap(string path)
{
	fileloader.OutputMap(_grid,path);
}

void TileMap::LoadMap(string path)
{
	_grid.erase(_grid.begin(), _grid.end());
	_grid = fileloader.GetMap(path);

	if (_grid[0]->GetTileSprite()->GetPostion().y < 49)//update old maps to new position to not clip into top menu bar
	{
		UpdateOldMapToNewPositions();
	}
	if (_event != nullptr)
	{
		_event->type = sf::Event::Resized;

	}
}

void TileMap::FitToScreen(sf::RenderWindow* window)
{
	while (_grid[_grid.size()-1]->GetTileSprite()->GetPostion().x > (window->getSize().x ) )
	{
		float scaleAmount = 0.01;

		int y = 0;
		int x = 0;
		int index = 0;
		for (int j = 0; j < _gridSize.x; j++)
		{
			y = 49;
			for (int i = 0; i < _gridSize.y; i++)
			{
				_grid[index]->GetTileSprite()->SetPosition(x, y);
				_grid[index]->GetTileSprite()->SetScale(_grid[index]->GetTileSprite()->GetScaleValue() - scaleAmount);
				y += _grid[index]->GetTileSprite()->GetSprite().getGlobalBounds().height;
				index++;
			}
			x += _grid[0]->GetTileSprite()->GetSprite().getGlobalBounds().width;
		}
	}


	while (_grid[_grid.size() - 1]->GetTileSprite()->GetPostion().x < (window->getSize().x))
	{
		float scaleAmount = 0.01;

		int y = 0;
		int x = 0;
		int index = 0;
		for (int j = 0; j < _gridSize.x; j++)
		{
			y = 49;
			for (int i = 0; i < _gridSize.y; i++)
			{
				_grid[index]->GetTileSprite()->SetPosition(x, y);
				_grid[index]->GetTileSprite()->SetScale(_grid[index]->GetTileSprite()->GetScaleValue() + scaleAmount);
				y += _grid[index]->GetTileSprite()->GetSprite().getGlobalBounds().height;
				index++;
			}
			x += _grid[0]->GetTileSprite()->GetSprite().getGlobalBounds().width;
		}
	}
}

sf::Vector2i TileMap::GetGridSize()
{
	return _gridSize;
}

void TileMap::SetGridSize(sf::Vector2i gridSize)
{
	_gridSize = gridSize;
	UpdateGrid();
}

sf::Vector2f TileMap::GetTileSize()
{
	return _tileSourceRectSize;
}

void TileMap::setTileSize(sf::Vector2f tileSize)
{
	_tileSourceRectSize = tileSize;
	UpdateGrid();
}

void TileMap::DrawTileMap(sf::RenderWindow* window, sf::Event* event)
{
	_event = event;
	for (size_t i = 0; i < _grid.size(); i++)
	{
		window->draw(_grid[i]->GetTileSprite()->GetSprite());

	}

	for (int i = 0; i < _highLightedSqua.size(); i++)
	{
		window->draw(_highLightedSqua[i]);
	}

	if (_event->type == sf::Event::Resized)
	{
		FitToScreen(window);
	}
}

Sprite* TileMap::GetSpriteAtLocation(sf::Vector2f mousePos)
{
	for (size_t i = 0; i < _grid.size(); i++)
	{
		if (_grid[i]->GetTileSprite()->GetSprite().getGlobalBounds().contains(mousePos))
		{
			return _grid[i]->GetTileSprite();
			break;
		}
	}
}

void TileMap::ClearMapUsingSprite(Sprite* sprite)
{
	for (size_t i = 0; i < _grid.size(); i++)
	{
		_grid[i]->SetTileSprite(sprite);	
	}
}

void TileMap::DrawHighlightBoxAroundSelected(sf::Vector2f Pos, Sprite* sprite)
{
	for (size_t i = 0; i < _grid.size(); i++)
	{
		if (_grid[i]->GetTileSprite()->GetSprite().getGlobalBounds().contains(Pos))
		{
			sf::RectangleShape newRect;
			newRect.setSize(sf::Vector2f(_grid[i]->GetTileSprite()->GetWidth()-5, _grid[i]->GetTileSprite()->GetHeight()-5));
			newRect.setPosition(sf::Vector2f(_grid[i]->GetTileSprite()->GetSprite().getGlobalBounds().left +1, _grid[i]->GetTileSprite()->GetSprite().getGlobalBounds().top+1));
			newRect.setOutlineColor(sf::Color::Yellow);
			newRect.setOutlineThickness(3);
			newRect.setFillColor(sf::Color(0, 0, 0, 0));
			_highLightedSqua.push_back(newRect);
			_highLightedTiles.push_back(_grid[i]);
			Sleep(100);			
			break;
		}
	}


}

void TileMap::UpdateOldMapToNewPositions()
{
	for (size_t i = 0; i < _grid.size(); i++)
	{
		_grid[i]->GetTileSprite()->SetPositionY(_grid[i]->GetTileSprite()->GetPostion().y + 17);
	}
}
