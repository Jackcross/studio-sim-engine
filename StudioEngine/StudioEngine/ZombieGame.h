#pragma once
#include "TileMap.h"
#include "Button.h"
#include "LoadFromFile.h"
#include "Wave.h"
#include "GunManager.h"
class ZombieGame
{
public:
	ZombieGame(vector<Entity*> entites, TileMap* map);
	void Draw(sf::RenderWindow* gameWindow);
	void update(double elaspedTime, sf::Vector2f mousePos);
	void castEntites();
	void SetupText();
	void GetAmmo();
	void PlayerAndZombieCollision();
	void CreateNewGame();
	void mapCollisionTEST();
private:
	Wave* wave1;
	vector<Entity*> entities;

	Player* fighter;
	float playerHealth;

	TileMap* map;

	vector<Tile*> LAVATiles;
	vector<Object*> LAVATilesOBJ;

	GunManager* gun;
	LoadFromFile load;

	sf::Text ammoText;
	sf::Font MyFont;
	vector<Sprite*> ammoBox;

	sf::Text zombiesLeft;

	Object* healthBarOutline;
	Object* healthBar;

	int CurrentMap;
};

