#pragma once
#include "Structs.h"
#include "Sprite.h"
#include "TileMap.h"
#include "LoadFromFile.h"
#include "Button.h"
#include "TextBox.h"
#include "DropList.h"

class MapMenu
{
public:
	MapMenu(TileMap* map, sf::Font* font );

	void		Draw(sf::RenderWindow* gameWindow, vector<sf::RectangleShape*> LayoutShapes);
	void		Update(sf::Vector2f mousePos, sf::Event* event);

	void		DrawClosed(sf::RenderWindow* gameWindow, vector<sf::RectangleShape*> LayoutShapes);
	void		UpdateClosed(sf::Vector2f mousePos, sf::Event* event);

	void		DropORResetMouseSprite(vector<sf::RectangleShape*> LayoutShapes);//set texture rect to zero if button released and drop sprite onto screen space
	string		RemoveTxtFromString(string Text);

	void		WindowResize(sf::RenderWindow* gameWindow);

private:

	void			SetupReferencePositions();
	void			LoadGameSprite();
	void			SetupSprites();
	void			DrawHighBoxAndSetMouseSprite(sf::RenderWindow* gameWindow);
	bool			Collision(float x1, float y1, float width1, float hight1, float x2, float y2, float width2, float hight2);
	void			listSetup();
	void			ButtonSetup();
	void			TextboxSetup();
	void			LoadListUpdate();

	TileMap*		_map;
	vector<Sprite*> _VtilesSprites;
	sf::RectangleShape* _sideMenu;
	sf::RectangleShape* _topMenu;
	LoadFromFile	_fileLoader;

	Button*			savebtn;
	Button*			Clearbtn;
	TextBox*		saveTextBox;
	Button*			LoadNewSpritesbtn;
	int				ScreenWidth;
	int				ScreenHeight;

	DropList*		LoadList;
	DropList*		_isWallList;

	Sprite*			_clearSprite;
	Sprite*			_selectSprite;

	Sprite*			DisplaySelected;
	sf::Event*		event;
	sf::Text		DisplayedSpriteText;
	sf::Text		DisplayedSpriteNAME;
	sf::Vector2f	mousePosition;
	Sprite*			mouseSprite;
	sf::Font*		font;
};

