#include "LoadFromFile.h"
#include <sys/types.h>
#include <dirent.h>

std::vector<Tile*> LoadFromFile::grid;

LoadFromFile::LoadFromFile()
{
}

void LoadFromFile::OutputEntities(string path,vector<Entity*> Entites, vector<Sprite*> sprites)
{
	string fileName = "Resources/EntityList/" + path;

	ofstream myfile(fileName);
	if (myfile.is_open())
	{
		for (size_t i = 0; i < Entites.size(); i++)
		{
			myfile << "TYPE :" << Entites[i]->GetType() << "\n";
			myfile << "NAME :" << Entites[i]->GetName() << "\n";
			myfile << "SPRITE :" << Entites[i]->GetImage()->GetFilePath() << "\n";
			myfile << "POS :" << "(" << Entites[i]->GetPosition().x << "," << Entites[i]->GetPosition().y  << ")"  << "\n";
			myfile << "SOURCERECT : " << "(" << Entites[i]->GetImage()->GetSourceRect().x << "," << Entites[i]->GetImage()->GetSourceRect().y << ")" << "\n";
			myfile << "SCALE :" << Entites[i]->GetImage()->GetScaleValue() << "\n";
			myfile << "ROTA :" << Entites[i]->GetImage()->GetSprite().getRotation() << "\n\n";
		}
	}
	else cout << "Unable to open file";
}

void LoadFromFile::GetEntities( vector<Entity*>* AllEntities, vector<Player*>* Players, vector<NPC*>* NPCs, vector<Object*>* objects, string path)
{

	AllEntities->clear();
	if (Players != nullptr)
	{
		Players->clear();
		NPCs->clear();
		objects->clear();
	}

	string fileName = "Resources/EntityList/" + path;
	ifstream infile(fileName);

	string line;
	int populationNumber = 0;
	while (!infile.eof())
	{
		Entity* newEntity = new Entity();
		getline(infile, line);
		if (line == "")
			break;

		int TYPE = UNDEFINED_TYPE;
		
		if (line.at(0) == 'T')
		{
			unsigned last = line.find(':');
			string newString = line.substr(last + 1, last - 1);
			istringstream(newString) >> TYPE;
			getline(infile, line);
		}
		if (line.at(0) == 'N')
		{
			unsigned last = line.find(':');
			string newString = line.substr(last + 1);
			newEntity->SetName(newString);
			getline(infile, line);
		}
		if (line.at(1) == 'P')
		{
			unsigned last = line.find(':');
			string newString = line.substr(last + 1);
			Sprite* sprite = new Sprite("Resources/Textures/" + newString);
			newEntity->SetSprite(sprite);
			getline(infile, line);
		}
		if (line.at(0) == 'P')
		{
			unsigned first = line.find('(');
			unsigned last = line.find(',');
			string newString = line.substr(first + 1, +(last - first) - 1);
			int x;
			istringstream(newString) >> x;

			first = line.find(',');
			last = line.find(')');
			newString = line.substr(first + 1, +(last - first) - 1);
			int y;
			istringstream(newString) >> y;

			newEntity->SetPosition(sf::Vector2f(x, y));
			getline(infile, line);
		}
		if (line.at(1) == 'O')
		{
			unsigned first = line.find('(');
			unsigned last = line.find(',');
			string newString = line.substr(first + 1, +(last - first) - 1);
			int x;
			istringstream(newString) >> x;

			first = line.find(',');
			last = line.find(')');
			newString = line.substr(first + 1, +(last - first) - 1);
			int y;
			istringstream(newString) >> y;

			newEntity->GetImage()->SetSourceRect(sf::Vector2f(x, y));
			getline(infile, line);
		}
		if (line.at(0) == 'S')
		{
			float Scale;
			unsigned last = line.find(':');
			string newString = line.substr(last + 1, last - 1);
			istringstream(newString) >> Scale;
			newEntity->GetImage()->SetScale(Scale);
			getline(infile, line);
		}
		if (line.at(0) == 'R')
		{
			//need rotation setting box 
			getline(infile, line);
		}


		if (TYPE == OBJECT_TYPE)
		{
			Object* newObject = new Object();
			newObject->SetName(newEntity->GetName());
			newObject->SetSprite(newEntity->GetImage());
			newObject->SetPosition(newEntity->GetPosition());
			newObject->SetType(OBJECT_TYPE);
			newObject->GetImage()->SetScale(newEntity->GetImage()->GetScaleValue());
			newObject->GetImage()->SetSourceRect(newEntity->GetImage()->GetSourceRect());
			if (Players != nullptr)
			{
				objects->push_back(newObject);
			}
			

			//objects->push_back(dynamic_cast<Object*>(newEntity));
			AllEntities->push_back(newObject);
		}
		else if (TYPE == PLAYER_TYPE)
		{
			Player* newPlayer = new Player();
			newPlayer->SetName(newEntity->GetName());
			newPlayer->SetSprite(newEntity->GetImage());
			newPlayer->SetPosition(newEntity->GetPosition());
			newPlayer->SetType(PLAYER_TYPE);
			newPlayer->GetImage()->SetScale(newEntity->GetImage()->GetScaleValue());
			newPlayer->GetImage()->SetSourceRect(newEntity->GetImage()->GetSourceRect());
			if (Players != nullptr)
			{
				Players->push_back(newPlayer);
			}
			//newEntity->SetType(PLAYER_TYPE);
			//Players->push_back(dynamic_cast<Player*>(newEntity));
			AllEntities->push_back(newPlayer);
		}
		else if (TYPE == NPC_TYPE)
		{
			NPC* newNPC = new NPC();
			newNPC->SetName(newEntity->GetName());
			newNPC->SetSprite(newEntity->GetImage());
			newNPC->SetPosition(newEntity->GetPosition());
			newNPC->SetType(NPC_TYPE);
			newNPC->GetImage()->SetScale(newEntity->GetImage()->GetScaleValue());
			newNPC->GetImage()->SetSourceRect(newEntity->GetImage()->GetSourceRect());
			if (Players != nullptr)
			{
				NPCs->push_back(newNPC);
			}


			AllEntities->push_back(newNPC);
		}
		else if (TYPE == UNDEFINED_TYPE)
		{
			Entity* newUndefined = new Entity();
			newUndefined->SetName(newEntity->GetName());
			newUndefined->SetSprite(newEntity->GetImage());
			newUndefined->SetPosition(newEntity->GetPosition());
			newUndefined->SetType(UNDEFINED_TYPE);
			newUndefined->GetImage()->SetScale(newEntity->GetImage()->GetScaleValue());
			newUndefined->GetImage()->SetSourceRect(newEntity->GetImage()->GetSourceRect());
			AllEntities->push_back(newUndefined);

		}
	}
}

void LoadFromFile::GetEntities(vector<Entity*>* undefinedEntities, string path)
{
	GetEntities(undefinedEntities, nullptr, nullptr, nullptr, path);

}


vector<Sprite*> LoadFromFile::LoadMapSprites()
{
	string name = "Resources/MapTextures/";
	vector<string> v;
	DIR* dirp = opendir(name.c_str());
	struct dirent* dp;
	while ((dp = readdir(dirp)) != NULL) {
		v.push_back(dp->d_name);
	}
	closedir(dirp);

	v.erase(v.begin());//first two iteams are alawys "::" or "." dont know why, but remove. 
	v.erase(v.begin());

	vector<Sprite*> SpriteVec;

	for (size_t i = 0; i < v.size(); i++)
	{
		Sprite* newSprite = new Sprite("Resources/MapTextures/" + v[i]);
		SpriteVec.push_back(newSprite);

	}
	return SpriteVec;
}

void LoadFromFile::OutputMap(vector<Tile*> gird, string path)
{
	string fileName = "Resources/MapList/" + path + ".txt";

	ofstream myfile(fileName);
	if (myfile.is_open())
	{
		for (size_t i = 0; i < gird.size(); i++)
		{
			myfile << "SPRITE :" << gird[i]->GetTileSprite()->GetFilePath() <<"\n";
			myfile << "POS :" << "(" << gird[i]->GetTileSprite()->GetPostion().x << "," << gird[i]->GetTileSprite()->GetPostion().y << ")" << "\n";
			myfile << "SCALE :" << gird[i]->GetTileSprite()->GetScaleValue() << "\n\n";
		}
	}
	else cout << "Unable to open file";

}

vector<Tile*> LoadFromFile::GetMap(string path)
{
	string fileName = "Resources/MapList/" + path;
	ifstream infile(fileName);


	if (grid.size() != 0)
	{
		for (Tile* t : grid)
		{
			delete t;
		}
		grid.clear();
	}


	string line;
	int populationNumber = 0;
	while (!infile.eof())
	{
		Tile* tile = nullptr;
		getline(infile, line);
		if (line == "")
			break;

		if (line.at(0) == 'S')
		{
			unsigned last = line.find(':');
			string newString = line.substr(last + 1);
			tile = new Tile(newString);

			getline(infile, line);
		}
		if (line.at(0) == 'P')
		{
			unsigned first = line.find('(');
			unsigned last = line.find(',');
			string newString = line.substr(first + 1, +(last - first) - 1);
			int x;
			istringstream(newString) >> x;

			first = line.find(',');
			last = line.find(')');
			newString = line.substr(first + 1, +(last - first) - 1);
			int y;
			istringstream(newString) >> y;

			tile->GetTileSprite()->SetPosition(x, y);
			getline(infile, line);

		}
		if (line.at(0) == 'S' && line.at(1) == 'C')
		{
			float scale;
			unsigned last = line.find(':');
			string newString = line.substr(last + 1);
			istringstream(newString) >> scale;
			tile->GetTileSprite()->SetScale(scale);
			getline(infile, line);
		}
		grid.push_back(tile);		

	}
	
	return grid;
}

vector<string> LoadFromFile::GetMapFilePaths()
{
	string name = "Resources/MapList/";
	vector<string> v;
	DIR* dirp = opendir(name.c_str());
	struct dirent* dp;
	while ((dp = readdir(dirp)) != NULL) {
		v.push_back(dp->d_name);
	}
	closedir(dirp);

	v.erase(v.begin());//first two iteams are alawys "::" or "." dont know why, but remove. 
	v.erase(v.begin());

	return v;
}

vector<string> LoadFromFile::GetEntityFilePaths()
{
	string name = "Resources/EntityList/";
	vector<string> v;
	DIR* dirp = opendir(name.c_str());
	struct dirent* dp;
	while ((dp = readdir(dirp)) != NULL) {
		v.push_back(dp->d_name);
	}
	closedir(dirp);

	v.erase(v.begin());//first two iteams are alawys "::" or "." dont know why, but remove. 
	v.erase(v.begin());

	return v;
}

void LoadFromFile::MakeHeaderFile(vector<Entity*> Entites)
{
	string fileName = "CurrentEntites.h";

	ofstream myfile(fileName);
	if (myfile.is_open())
	{
		myfile << "#include \"Entity.h\" \n";
		myfile << "#include \"Structs.h\" \n";


		myfile << "class CurrentEntites"  << "\n";
		myfile << "{" << "\n";

		myfile << "public: \n";

		myfile << "CurrentEntites(); \n";


		myfile << "};" << "\n";

		myfile << "CurrentEntites::CurrentEntites()\n ";

		myfile << "{" << "\n";
		myfile << "\t vector<Entity*> _entities; \n";

		myfile << "\t Entity* newEntity = new Entity(); \n\n";

		for (size_t i = 0; i < Entites.size(); i++)
		{
			myfile << "\t //Entity Number : " << i << "\n";
			myfile << "\t Sprite* Sprite"<< i <<" = new Sprite(\"Resources/Textures/" << Entites[i]->GetImage()->GetFilePath() << "\");" << "\n";
			myfile << "\t newEntity->SetName(\"" << Entites[i]->GetName() << "\");" << "\n";
			myfile << "\t newEntity->SetSprite(Sprite" << i << ");" << "\n";
			myfile << "\t newEntity->GetImage()->SetFilePath(" << "\"" << Entites[i]->GetImage()->GetFilePath() << "\"" << ");" << "\n";
			myfile << "\t newEntity->SetPosition(sf::Vector2f(" << Entites[i]->GetPosition().x << "," << Entites[i]->GetPosition().y << "));" << "\n";
			myfile << "\t newEntity->SetType((Entity_Type)"<< Entites[i]->GetType() << ");" << "\n";
			myfile << "\t newEntity->GetImage()->SetScale(" << Entites[i]->GetImage()->GetScaleValue() << ");" << "\n";
			myfile << "\t newEntity->GetImage()->SetSourceRect(sf::Vector2f(" << Entites[i]->GetImage()->GetSourceRect().x << "," << Entites[i]->GetImage()->GetSourceRect().y << "));" << "\n";

			myfile << "\n";
		}
		myfile << "}" << "\n";

	}
	else cout << "Unable to open file";
}

vector<Sprite*> LoadFromFile::GetSprites()
{
	string name = "Resources/Textures/";
	vector<string> v;
	DIR* dirp = opendir(name.c_str());
	struct dirent* dp;
	while ((dp = readdir(dirp)) != NULL) {
		v.push_back(dp->d_name);
	}
	closedir(dirp);

	v.erase(v.begin());//first two iteams are alawys "::" or "." dont know why, but remove. 
	v.erase(v.begin());

	vector<Sprite*> SpriteVec;

	for (size_t i = 0; i < v.size(); i++)
	{
		Sprite* newSprite = new Sprite("Resources/Textures/" + v[i]);
		newSprite->SetFilePath(v[i]);
		SpriteVec.push_back(newSprite);

	}
	return SpriteVec;
}


